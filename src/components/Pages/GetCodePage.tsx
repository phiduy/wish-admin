import React from 'react';
import { Row, Col, Typography, Layout } from 'antd';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';

const { Paragraph } = Typography;

const { Content } = Layout;
export const GetCodePage = () => {
	const { search } = useLocation();
	return (
		<Content
			style={{
				background: '#fff',
				boxShadow: '1px 5px 24px 0 rgba(68, 102, 242, 0.05)',
				minHeight: '80vh',
			}}
		>
			<Row align="middle">
				<Col span={6} offset={9}>
					<div className="text-center mb-1">
						<h5 className="mb-1 mt-3 font-weight-bold">Wish</h5>
						<p className="text-muted">Your code below</p>
					</div>
					<Paragraph
						copyable={true}
						className="text-center"
						style={{
							border: '1px solid  rgba(0,0,0,0.2)',
							borderRadius: 8,
							padding: '.2rem .5rem',
						}}
					>
						{queryString.parse(search).code}
					</Paragraph>
				</Col>
			</Row>
		</Content>
	);
};
