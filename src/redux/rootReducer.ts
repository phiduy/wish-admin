/**
 * @file Root reducers
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import IStore from './store/IStore';

// Place for reducers' app
import RegisterPage, { name as nameOfRegisterPage } from '../modules/Register';
import LoginPage, { name as nameOfLoginPage } from '../modules/Login';
import ShopPage, { name as nameOfShopPage } from '../modules/Shop';
import UserPage, { name as nameOfUserPage } from '../modules/User';
import MainLayout, { name as nameOfMainLayout } from '../layouts/MainLayout';

/*----Reducers List-----------------*/
const reducers = {
	[nameOfUserPage]: UserPage,
	[nameOfRegisterPage]: RegisterPage,
	[nameOfShopPage]: ShopPage,
	[nameOfLoginPage]: LoginPage,
	[nameOfMainLayout]: MainLayout,
};

const rootReducer = (history: History) =>
	combineReducers<IStore>({
		...reducers,
		router: connectRouter(history),
	});

export default rootReducer;
