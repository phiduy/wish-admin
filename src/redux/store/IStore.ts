import { ILogInState, name as LoginPageState } from '../../modules/Login';
import { IRegisterState, name as RegisterPageState } from '../../modules/Register';
import { IShopState, name as ShopPageState } from '../../modules/Shop';
import { IUserState, name as UserPageState } from '../../modules/User';
import { IMainLayoutState, name as MainLayoutState } from '../../layouts/MainLayout';

export default interface IStore {
	[LoginPageState]: ILogInState;
	[ShopPageState]: IShopState;
	[UserPageState]: IUserState;
	[RegisterPageState]: IRegisterState;
	[MainLayoutState]: IMainLayoutState;
	router: any;
}
