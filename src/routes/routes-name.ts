export const routeName = {
	login: '/login',
	register: '/register',
	resetPassword: '/reset-password',
	shop: '/shop',
	order: '/order',
	user: '/user',
	shopCode: '/shop-code',
	notAccept: '/not-accept',
};
