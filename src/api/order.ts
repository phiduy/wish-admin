import { request } from '../config/axios';
import { IOrder, ORDER_TAB } from '../modules/Shop/model/IShopState';

export const getOrdersMultipleShop = (data: {
	start: number;
	type: ORDER_TAB;
	sort: 1 | -1;
	limit: number;
	order?: string;
	listShop?: string[];
	sortCol: 'last_updated' | 'order_time';
}) => {
	const endpoint = `/shops/all_orders?start=${data.start}&limit=${data.limit}&type=${
		data.type === 'history' ? '' : data.type
	}&sort=${data.sort}&sortCol=${data.sortCol}${
		data.order !== undefined ? `&order=${data.order}` : ''
	}`;
	const body = data.listShop ? { listShop: data.listShop } : { all: true };
	return request(endpoint, 'POST', body);
};

export const getOrders = (data: {
	shopId: string;
	start: number;
	limit: number;
	order: string;
}) => {
	const endpoint = `/shops/${data.shopId}/orders?start=${data.start}&limit=${data.limit}${
		data.order !== undefined ? `&order=${data.order}` : ''
	}`;
	return request(endpoint, 'GET', null);
};

export const getNoteOrders = (data: {
	shopId: string;
	start: number;
	limit: number;
	order: string;
}) => {
	const endpoint = `/shops/${data.shopId}/noted_orders?start=${data.start}&limit=${data.limit}${
		data.order !== undefined ? `&order=${data.order}` : ''
	}`;
	return request(endpoint, 'GET', null);
};

export const getOriginalProductImage = (data: { shopId: string; productId: string }) => {
	const endpoint = `/shops/${data.shopId}/products/${data.productId}/image`;
	return request(endpoint, 'GET', null);
};

export const putNoteOrder = (data: { shopId: string; orderId: string; isNoted: boolean }) => {
	const endpoint = `/shops/${data.shopId}/orders/${data.orderId}/note`;
	return request(endpoint, 'PUT', { isNoted: data.isNoted });
};

export const getDeliveryCountries = (data: { shopId: string }) => {
	const endpoint = `/shops/${data.shopId}/delivery_countries`;
	return request(endpoint, 'GET', null);
};

export const getShippingCarrierByLocale = (data: { shopId: string; locale: string }) => {
	const endpoint = `/shops/${data.shopId}/shipping_carriers?locale=${data.locale}`;
	return request(endpoint, 'GET', null);
};

export const getActionRequiredOrder = (data: {
	shopId: string;
	limit: number;
	start: number;
	order: string;
}) => {
	const endpoint = `/shops/${data.shopId}/fullfill_orders?start=${data.start}&limit=${
		data.limit
	}${data.order !== undefined ? `&order=${data.order}` : ''}`;
	return request(endpoint, 'GET', null);
};

export const postFullFillOrder = (data: { shopId: string; order: IOrder }) => {
	console.log('data', data);
	const endpoint = `/shops/${data.shopId}/fullfill_orders`;
	return request(endpoint, 'POST', { ...data.order });
};

export const postRefundOrder = (data: { shopId: string; orderId: string; reasonCode: number }) => {
	const endpoint = `/shops/${data.shopId}/refund_orders`;
	return request(endpoint, 'POST', { orderId: data.orderId, reasonCode: data.reasonCode });
};

export const postModifyShippedOrder = (data: { shopId: string; order: IOrder }) => {
	console.log('data', data);
	const endpoint = `/shops/${data.shopId}/modify_shipped_orders`;
	return request(endpoint, 'POST', { ...data.order });
};
