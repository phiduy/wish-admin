import * as React from 'react';
import { connect } from 'react-redux';
import { ConfigProvider } from 'antd';
import IStore from './redux/store/IStore';
import { LoadingScreen } from './components';
import { AuthLayout } from './layouts/index';
import { mainRoutes, authRoutes } from './routes';
import MainLayout from './layouts/MainLayout/components/MainLayoutContainer';
import { ILogInState } from './modules/Login';
import { BrowserRouter as Router } from 'react-router-dom';

interface IProps {
	LoginState: ILogInState;
	router: any;
}
const App: React.FC<IProps> = (props) => {
	const { isLoading, accessToken } = props.LoginState;
	return (
		<React.Fragment>
			<ConfigProvider>
				<Router>
					<React.Suspense fallback={<LoadingScreen size="large" />}>
						{isLoading ? (
							<LoadingScreen size="large" />
						) : accessToken !== null ? (
							<MainLayout routes={mainRoutes} />
						) : (
							<AuthLayout routes={authRoutes} />
						)}
					</React.Suspense>
				</Router>
			</ConfigProvider>
		</React.Fragment>
	);
};

const mapStateToProps = (store: IStore) => ({
	LoginState: store.LoginPage,
	router: store.router,
});
export default connect(mapStateToProps)(App);
