/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: MAIN_LAYOUT_MODAL;
	};
}

export interface IHandleLogout extends Action {
	readonly type: Keys.HANDLE_LOGOUT;
}
