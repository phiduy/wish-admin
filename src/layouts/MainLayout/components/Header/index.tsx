import React from 'react';
import { Menu, Layout } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import './index.scss';
import { Link, useLocation } from 'react-router-dom';
import { IMainLayoutProps } from '../../model/IMainLayoutProps';

const { SubMenu } = Menu;
const { Header } = Layout;
export const HeaderBar = (props: IMainLayoutProps) => {
	const { pathname } = useLocation();
	const [selectedKey, setSelectedKey] = React.useState(pathname !== '/user' ? '/shop' : pathname);
	const { role } = props.store.LoginPage;
	const handleClick = (e: any) => {
		setSelectedKey(e.key);
	};
	return (
		<React.Fragment>
			<div className="header_dash__container">
				<Header>
					<Menu
						theme="light"
						onClick={handleClick}
						selectedKeys={[selectedKey]}
						mode="horizontal"
					>
						<Menu.Item key="/shop">
							<Link to="/shop">Shop</Link>
						</Menu.Item>
						{role === 'Admin' && (
							<Menu.Item key="/user">
								<Link to="/user">User</Link>
							</Menu.Item>
						)}
						<span className="mr-auto" />
						<SubMenu key="account" icon={<UserOutlined />}>
							<Menu.Item key="setting:4" onClick={() => props.actions.handleLogout()}>
								Log out
							</Menu.Item>
						</SubMenu>
					</Menu>
				</Header>
			</div>
		</React.Fragment>
	);
};
