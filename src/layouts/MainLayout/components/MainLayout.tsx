import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Layout, BackTop } from 'antd';
import { IMainLayoutProps } from '../model/IMainLayoutProps';
import { RouteConfig } from '../../../routes';
import { HeaderBar } from './Header';
import { useSelector } from 'react-redux';
import IStore from '../../../redux/store/IStore';

import './MainLayout.scss';

const { Content } = Layout;

interface IProps extends IMainLayoutProps {
	routes: RouteConfig[];
}
export const MainLayout: React.FC<IProps> = (props) => {
	const { routes } = props;
	const LoginState = useSelector((state: IStore) => state.LoginPage);

	return (
		<React.Fragment>
			<HeaderBar {...props} />
			<Layout className="workspace">
				<Layout>
					<Content>
						<div className="page__inner">
							<Switch>
								{routes.map((item) => {
									if (item.permission === 'Admin') {
										if (item.permission === LoginState.role) {
											return (
												<Route
													key={item.path}
													path={item.path}
													component={item.component}
												/>
											);
										}
										return null;
									}
									return (
										<Route
											key={item.path}
											path={item.path}
											component={item.component}
										/>
									);
								})}
								{props.routes.length > 0 ? (
									<Redirect to={props.routes[0].path} />
								) : null}
								<Redirect from="*" to="/404" />
							</Switch>
							{props.children}
						</div>
					</Content>
				</Layout>
				<BackTop />
			</Layout>
		</React.Fragment>
	);
};
