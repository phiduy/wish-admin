import { takeEvery } from 'redux-saga/effects';
import Keys from '../actionTypeKeys';
// import * as AuthApi from '../../../api/auth';

// eslint-disable-next-line
function* handleLogoutAccount() {
	localStorage.clear();
	window.location.reload();
}
/*-----------------------------------------------------------------*/
function* watchLogoutAccount() {
	yield takeEvery(Keys.HANDLE_LOGOUT, handleLogoutAccount);
}

/*-----------------------------------------------------------------*/
const sagas = [watchLogoutAccount];
export default sagas;
