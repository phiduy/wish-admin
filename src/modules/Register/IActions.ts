/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IUserRegisterInfo } from './model/IRegisterState';

//#region User Register IActions
export interface IUserRegister extends Action {
	readonly type: Keys.USER_REGISTER;
	payload: IUserRegisterInfo;
}

export interface IUserRegisterSuccess extends Action {
	readonly type: Keys.USER_REGISTER_SUCCESS;
	payload: {
		userInfo: {
			name: string;
			role: string;
		};
	};
}

export interface IUserRegisterFail extends Action {
	readonly type: Keys.USER_REGISTER_FAIL;
	payload?: {
		errors: string[];
	};
}
//#endregion
