import React from 'react';
import { Input, Button, Form } from 'antd';
import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import { IRegisterProps } from '../../model/IRegisterProps';

interface IProps extends IRegisterProps {}

interface IInputs {
	lastName: string;
	email: string;
	password: string;
	firstName: string;
}

export const RegisterForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.RegisterPage;

	const onFinish = (data: IInputs) => {
		const { lastName, email, password, firstName } = data;
		props.actions.userRegister({
			lastName,
			email,
			password,
			firstName,
		});
	};
	return (
		<Form form={formInstance} layout="vertical" onFinish={onFinish}>
			<div className="d-flex">
				<Form.Item
					label="Last name"
					name="lastName"
					className="w-100 mr-2"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Input
							prefix={<UserOutlined />}
							placeholder="Enter last name"
							disabled={isProcessing}
						/>
					}
				/>
				<Form.Item
					label="First name"
					name="firstName"
					className="w-100"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
				>
					<Input
						prefix={<UserOutlined />}
						placeholder="Enter first name"
						disabled={isProcessing}
					/>
				</Form.Item>
			</div>
			<Form.Item
				label="Email"
				name="email"
				rules={[
					{
						type: 'email',
					},
					{
						required: true,
					},
					{
						max: 256,
					},
				]}
			>
				<Input
					prefix={<MailOutlined />}
					placeholder="Enter your email"
					disabled={isProcessing}
				/>
			</Form.Item>

			<Form.Item
				name="password"
				label="Password"
				rules={[
					{
						required: true,
					},
					{
						max: 256,
					},
					{
						min: 6,
					},
				]}
				hasFeedback={true}
			>
				<Input.Password
					prefix={<LockOutlined />}
					placeholder="Enter your password"
					disabled={isProcessing}
				/>
			</Form.Item>

			<Form.Item
				label="Confirm Password"
				name="confirm"
				dependencies={['password']}
				hasFeedback={true}
				rules={[
					{
						required: true,
					},
					{
						max: 256,
					},
					({ getFieldValue }) => ({
						validator(rule, value) {
							if (!value || getFieldValue('password') === value) {
								return Promise.resolve();
							}
							return Promise.reject(
								'The two passwords that you entered do not match!'
							);
						},
					}),
				]}
			>
				<Input.Password
					prefix={<LockOutlined />}
					placeholder="Enter confirm password"
					disabled={isProcessing}
				/>
			</Form.Item>

			<Button
				block={true}
				className="text-capitalize"
				type="primary"
				htmlType="submit"
				loading={isProcessing}
				disabled={isProcessing}
			>
				Register
			</Button>
		</Form>
	);
};
