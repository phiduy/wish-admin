import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IRegisterProps {
	store: IStore;
	actions: typeof Actions;
}
