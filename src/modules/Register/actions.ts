import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IUserRegisterInfo } from './model/IRegisterState';

//#region User Register Actions
export const userRegister = (data: IUserRegisterInfo): IActions.IUserRegister => {
	return {
		type: Keys.USER_REGISTER,
		payload: data,
	};
};

export const userRegisterSuccess = (res: any): IActions.IUserRegisterSuccess => {
	return {
		type: Keys.USER_REGISTER_SUCCESS,
		payload: res,
	};
};

export const userRegisterFail = (res: string[]): IActions.IUserRegisterFail => {
	return {
		type: Keys.USER_REGISTER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
