import { call, put, takeEvery } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';

// Handle User Login
function* handleUserLogin(action: any) {
	try {
		const res = yield call(AuthApi.login, action.payload);
		if (res.status === 200) {
			const { token, role } = res.data;
			if (role !== undefined) {
				localStorage.setItem('role', role);
			}
			localStorage.setItem('accessToken', token);
			message.success('Login Successful', 1);
			yield put(actions.userLoginSuccess(res.data));
		} else {
			const messageError = 'Wrong password';
			message.error(messageError, 2);
			throw new Error(messageError);
		}
	} catch (error) {
		yield put(actions.userLoginFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserLogin() {
	yield takeEvery(Keys.USER_LOGIN, handleUserLogin);
}

/*-----------------------------------------------------------------*/
const sagas = [watchUserLogin];
export default sagas;
