import React from 'react';
import { Input, Button, Form } from 'antd';
import { UserOutlined, LockOutlined, EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { ILogInProps } from '../../model/ILoginProps';
import { IUserLoginInfo } from '../../model/ILoginState';

interface IProps extends ILogInProps {}

export const LoginForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.LoginPage;

	const onFinish = (data: IUserLoginInfo) => {
		props.actions.userLogin(data);
	};
	return (
		<Form form={formInstance} layout="vertical" onFinish={onFinish}>
			<Form.Item
				label="Email"
				name="email"
				hasFeedback={true}
				rules={[
					{
						required: true,
					},
					{
						max: 256,
					},
					{
						type: 'email',
					},
				]}
				children={
					<Input
						placeholder="Enter email"
						prefix={<UserOutlined />}
						allowClear={true}
						disabled={isProcessing}
					/>
				}
			/>
			<Form.Item
				label="Password"
				name="password"
				dependencies={['password']}
				hasFeedback={true}
				rules={[
					{
						required: true,
					},
					{
						max: 256,
					},
					{
						min: 6,
					},
				]}
				children={
					<Input.Password
						placeholder="Enter password"
						prefix={<LockOutlined />}
						iconRender={(visible) =>
							visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
						}
						disabled={isProcessing}
					/>
				}
			/>

			<Button
				block={true}
				className="text-capitalize"
				type="primary"
				htmlType="submit"
				loading={isProcessing}
			>
				Log In
			</Button>
		</Form>
	);
};
