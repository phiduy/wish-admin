import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IUserState, initialState, USER_MODAL, USER_CLEAR } from './model/IUserState';

export const name = 'UsersPage';

export const reducer: Reducer<IUserState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);
		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_USER:
			return onHandleCurrentUser(state, action);

		case Keys.GET_USERS:
			return onGetUsers(state, action);
		case Keys.GET_USERS_SUCCESS:
			return onGetUsersSuccess(state, action);
		case Keys.GET_USERS_FAIL:
			return onGetUsersFail(state, action);

		case Keys.DELETE_USER:
			return onDeleteUser(state, action);
		case Keys.DELETE_USER_SUCCESS:
			return onDeleteUserSuccess(state, action);
		case Keys.DELETE_USER_FAIL:
			return onDeleteUserFail(state, action);

		case Keys.CHANGE_USER_PASSWORD:
			return onChangeUserPassword(state, action);
		case Keys.CHANGE_USER_PASSWORD_SUCCESS:
			return onChangeUserPasswordSuccess(state, action);
		case Keys.CHANGE_USER_PASSWORD_FAIL:
			return onChangeUserPasswordFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: IUserState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case USER_MODAL.CHANGE_PASSWORD:
			return {
				...state,
				toggleModalChangeUserPassword: !state.toggleModalChangeUserPassword,
			};

		default:
			return {
				...state,
			};
	}
};
const onHandleClear = (state: IUserState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case USER_CLEAR.DELETE_USER:
			return {
				...state,
				deleteUserSuccess: !state.deleteUserSuccess,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleCurrentUser = (state: IUserState, action: IActions.IHandleCurrentUser) => {
	const { type, userInfo } = action.payload;
	switch (type) {
		case 'detail':
			return {
				...state,
				currentUserInfo: userInfo,
			};

		default:
			return {
				...state,
				currentUserInfo: userInfo,
			};
	}
};

const onGetUsers = (state: IUserState, action: IActions.IGetUsers) => {
	return {
		...state,
		isLoadingUsers: true,
	};
};
const onGetUsersSuccess = (state: IUserState, action: IActions.IGetUsersSuccess) => {
	const { items, total_item } = action.payload;
	items.forEach((item, index) => {
		item['key'] = index;
	});

	return {
		...state,
		isLoadingUsers: false,
		userRecords: items,
		totalRecord: total_item,
	};
};
const onGetUsersFail = (state: IUserState, action: IActions.IGetUsersFail) => {
	return {
		...state,
		isLoadingUsers: false,
	};
};

const onDeleteUser = (state: IUserState, action: IActions.IDeleteUser) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onDeleteUserSuccess = (state: IUserState, action: IActions.IDeleteUserSuccess) => {
	return {
		...state,
		isProcessing: false,
		deleteUserSuccess: true,
	};
};
const onDeleteUserFail = (state: IUserState, action: IActions.IDeleteUserFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onChangeUserPassword = (state: IUserState, action: IActions.IChangeUserPassword) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onChangeUserPasswordSuccess = (
	state: IUserState,
	action: IActions.IChangeUserPasswordSuccess
) => {
	return {
		...state,
		isProcessing: false,
		toggleModalChangeUserPassword: false,
		currentUserInfo: undefined,
	};
};
const onChangeUserPasswordFail = (state: IUserState, action: IActions.IChangeUserPasswordFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
