import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IUserInfo, USER_CLEAR, USER_MODAL } from './model/IUserState';

export const handleClear = (data: { type: USER_CLEAR }): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type: data.type,
		},
	};
};

export const toggleModal = (data: { type: USER_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type: data.type,
		},
	};
};

export const handleCurrentUser = (data: {
	userInfo: IUserInfo;
	type: 'detail' | 'update';
}): IActions.IHandleCurrentUser => {
	return {
		type: Keys.HANDLE_CURRENT_USER,
		payload: data,
	};
};

//#region GET Users Actions
export const getUsers = (data: { page: number; limit?: number }): IActions.IGetUsers => {
	return {
		type: Keys.GET_USERS,
		payload: data,
	};
};

export const getUsersSuccess = (res: {
	current_page: number;
	items: IUserInfo[];
	per_page: number;
	total_item: number;
	total_page: number;
}): IActions.IGetUsersSuccess => {
	return {
		type: Keys.GET_USERS_SUCCESS,
		payload: res,
	};
};

export const getUsersFail = (res: string[]): IActions.IGetUsersFail => {
	return {
		type: Keys.GET_USERS_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region DELETE User Actions
export const deleteUser = (data: { _id: string }): IActions.IDeleteUser => {
	return {
		type: Keys.DELETE_USER,
		payload: data,
	};
};

export const deleteUserSuccess = (): IActions.IDeleteUserSuccess => {
	return {
		type: Keys.DELETE_USER_SUCCESS,
	};
};

export const deleteUserFail = (res: string[]): IActions.IDeleteUserFail => {
	return {
		type: Keys.DELETE_USER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Change User Password Actions
export const changeUserPassword = (data: {
	email: string;
	password: string;
}): IActions.IChangeUserPassword => {
	return {
		type: Keys.CHANGE_USER_PASSWORD,
		payload: data,
	};
};

export const changeUserPasswordSuccess = (): IActions.IChangeUserPasswordSuccess => {
	return {
		type: Keys.CHANGE_USER_PASSWORD_SUCCESS,
	};
};

export const changeUserPasswordFail = (res: any): IActions.IChangeUserPasswordFail => {
	return {
		type: Keys.CHANGE_USER_PASSWORD_FAIL,
		payload: res,
	};
};
//#endregion
