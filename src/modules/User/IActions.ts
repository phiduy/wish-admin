/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IUserInfo, USER_CLEAR, USER_MODAL } from './model/IUserState';

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: USER_CLEAR;
	};
}
export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: USER_MODAL;
	};
}

export interface IHandleCurrentUser extends Action {
	readonly type: Keys.HANDLE_CURRENT_USER;
	payload: {
		userInfo: IUserInfo;
		type: 'detail' | 'update';
	};
}
//#region Change User Password IActions
export interface IChangeUserPassword extends Action {
	readonly type: Keys.CHANGE_USER_PASSWORD;
	payload: {
		email: string;
		password: string;
	};
}

export interface IChangeUserPasswordSuccess extends Action {
	readonly type: Keys.CHANGE_USER_PASSWORD_SUCCESS;
}

export interface IChangeUserPasswordFail extends Action {
	readonly type: Keys.CHANGE_USER_PASSWORD_FAIL;
	payload?: {
		errors: string[];
	};
}
//#endregion

//#region GET Users IActions
export interface IGetUsers extends Action {
	readonly type: Keys.GET_USERS;
	payload: { page: number; limit?: number };
}

export interface IGetUsersSuccess extends Action {
	readonly type: Keys.GET_USERS_SUCCESS;
	payload: {
		current_page: number;
		items: IUserInfo[];
		per_page: number;
		total_item: number;
		total_page: number;
	};
}

export interface IGetUsersFail extends Action {
	readonly type: Keys.GET_USERS_FAIL;
	payload?: {
		errors: string[];
	};
}
//#endregion

//#region DELETE User IActions
export interface IDeleteUser extends Action {
	readonly type: Keys.DELETE_USER;
	payload: { _id: string };
}

export interface IDeleteUserSuccess extends Action {
	readonly type: Keys.DELETE_USER_SUCCESS;
}

export interface IDeleteUserFail extends Action {
	readonly type: Keys.DELETE_USER_FAIL;
	payload?: {
		errors: string[];
	};
}
//#endregion
