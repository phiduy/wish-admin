/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import {
	IShopInfo,
	IOrder,
	IShopStaff,
	IShopRecord,
	SHOP_CLEAR,
	SHOP_MODAL,
	SHOP_VIEW,
	IOrderRecord,
	ORDER_TAB,
} from './model/IShopState';

export interface IToggleView extends Action {
	readonly type: Keys.TOGGLE_VIEW;
	payload: {
		type: SHOP_VIEW;
	};
}

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: SHOP_MODAL;
	};
}

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: SHOP_CLEAR;
	};
}
export interface IHandleCurrentOrder extends Action {
	readonly type: Keys.HANDLE_CURRENT_ORDER;
	payload: {
		order: IOrderRecord;
		type: 'ship' | 'detail';
	};
}

export interface IHandleCurrentStaff extends Action {
	readonly type: Keys.HANDLE_CURRENT_STAFF;
	payload: {
		staff: IShopStaff;
		type: 'update' | 'delete' | 'detail';
	};
}
export interface IHandleCurrentShop extends Action {
	readonly type: Keys.HANDLE_CURRENT_SHOP;
	payload: {
		shopInfo: IShopRecord;
		type: 'update' | 'delete' | 'detail' | 'sync';
	};
}

//#region GET Shops IActions
export interface IGetShops extends Action {
	readonly type: Keys.GET_SHOPS;
	payload: {
		page: number;
		limit?: number;
	};
}

export interface IGetShopsSuccess extends Action {
	readonly type: Keys.GET_SHOPS_SUCCESS;
	payload: {
		current_page: number;
		items: IShopRecord[];
		per_page: number;
		total_item: number;
		total_page: number;
	};
}

export interface IGetShopsFail extends Action {
	readonly type: Keys.GET_SHOPS_FAIL;
	payload?: string;
}
//#endregion

//#region GET Sync Data IActions
export interface IGetSyncData extends Action {
	readonly type: Keys.GET_SYNC_DATA;
	payload: {
		shopId: string;
		date: string;
	};
}

export interface IGetSyncDataSuccess extends Action {
	readonly type: Keys.GET_SYNC_DATA_SUCCESS;
	payload: {
		current_page: number;
		items: IShopRecord[];
		per_page: number;
		total_item: number;
		total_page: number;
	};
}

export interface IGetSyncDataFail extends Action {
	readonly type: Keys.GET_SYNC_DATA_FAIL;
	payload?: string;
}
//#endregion

//#region GET Shop Detail IActions
export interface IGetShopDetail extends Action {
	readonly type: Keys.GET_SHOP_DETAIL;
	payload: {
		shopId: string;
	};
}

export interface IGetShopDetailSuccess extends Action {
	readonly type: Keys.GET_SHOP_DETAIL_SUCCESS;
	payload: IShopRecord;
}

export interface IGetShopDetailFail extends Action {
	readonly type: Keys.GET_SHOP_DETAIL_FAIL;
	payload?: string;
}
//#endregion

//#region CREATE Shop IActions
export interface ICreateShop extends Action {
	readonly type: Keys.CREATE_SHOP;
	payload: IShopInfo;
}

export interface ICreateShopSuccess extends Action {
	readonly type: Keys.CREATE_SHOP_SUCCESS;
	payload: any;
}

export interface ICreateShopFail extends Action {
	readonly type: Keys.CREATE_SHOP_FAIL;
	payload?: string;
}
//#endregion

//#region UPDATE Shop IActions
export interface IUpdateShop extends Action {
	readonly type: Keys.UPDATE_SHOP;
	payload: {
		shopId: string;
		shopInfo: IShopInfo;
	};
}

export interface IUpdateShopSuccess extends Action {
	readonly type: Keys.UPDATE_SHOP_SUCCESS;
	payload: any;
}

export interface IUpdateShopFail extends Action {
	readonly type: Keys.UPDATE_SHOP_FAIL;
	payload?: string;
}
//#endregion

//#region DELETE Shop IActions
export interface IDeleteShop extends Action {
	readonly type: Keys.DELETE_SHOP;
	payload: {
		shopId: string;
	};
}

export interface IDeleteShopSuccess extends Action {
	readonly type: Keys.DELETE_SHOP_SUCCESS;
}

export interface IDeleteShopFail extends Action {
	readonly type: Keys.DELETE_SHOP_FAIL;
	payload?: string;
}
//#endregion

//#region GET Code Url IActions
export interface IGetCodeUrl extends Action {
	readonly type: Keys.GET_CODE_URL;
	payload: {
		shopId: string;
	};
}

export interface IGetCodeUrlSuccess extends Action {
	readonly type: Keys.GET_CODE_URL_SUCCESS;
	payload: {
		url: string;
	};
}

export interface IGetCodeUrlFail extends Action {
	readonly type: Keys.GET_CODE_URL_FAIL;
	payload?: string;
}
//#endregion

//#region Add Code IActions
export interface IAddCode extends Action {
	readonly type: Keys.ADD_CODE;
	payload: {
		shopId: string;
		code: string;
	};
}

export interface IAddCodeSuccess extends Action {
	readonly type: Keys.ADD_CODE_SUCCESS;
	payload: string;
}

export interface IAddCodeFail extends Action {
	readonly type: Keys.ADD_CODE_FAIL;
	payload?: string;
}
//#endregion

//#region Get Refresh Token Url IActions
export interface IGetRefreshTokenUrl extends Action {
	readonly type: Keys.GET_REFRESH_TOKEN_URL;
	payload: {
		shopId: string;
	};
}
export interface IGetRefreshTokenUrlSuccess extends Action {
	readonly type: Keys.GET_REFRESH_TOKEN_URL_SUCCESS;
	payload: {
		url: string;
	};
}
export interface IGetRefreshTokenUrlFail extends Action {
	readonly type: Keys.GET_REFRESH_TOKEN_URL_FAIL;
	payload?: string;
}
//#endregion

//#region Update Refresh Token Url IActions
export interface IUpdateAccessToken extends Action {
	readonly type: Keys.UPDATE_ACCESS_TOKEN;
	payload: {
		shopId: string;
		accessToken: string;
		expiredTime: string;
	};
}
export interface IUpdateAccessTokenSuccess extends Action {
	readonly type: Keys.UPDATE_ACCESS_TOKEN_SUCCESS;
}
export interface IUpdateAccessTokenFail extends Action {
	readonly type: Keys.UPDATE_ACCESS_TOKEN_FAIL;
	payload?: string;
}
//#endregion

//#region GET Staff IActions
export interface IGetStaff extends Action {
	readonly type: Keys.GET_STAFF;
	payload: {
		shopId: string;
	};
}
export interface IGetStaffSuccess extends Action {
	readonly type: Keys.GET_STAFF_SUCCESS;
	payload: IShopStaff[];
}
export interface IGetStaffFail extends Action {
	readonly type: Keys.GET_STAFF_FAIL;
	payload?: string;
}
//#endregion

//#region CREATE Staff IActions
export interface ICreateStaff extends Action {
	readonly type: Keys.CREATE_STAFF;
	payload: {
		shopId: string;
		staff: IShopStaff;
	};
}

export interface ICreateStaffSuccess extends Action {
	readonly type: Keys.CREATE_STAFF_SUCCESS;
	payload: any;
}

export interface ICreateStaffFail extends Action {
	readonly type: Keys.CREATE_STAFF_FAIL;
	payload?: {
		errors: {
			message: string;
		};
	};
}
//#endregion

//#region DELETE Staff IActions
export interface IDeleteStaff extends Action {
	readonly type: Keys.DELETE_STAFF;
	payload: {
		shopId: string;
		staffId: string;
	};
}

export interface IDeleteStaffSuccess extends Action {
	readonly type: Keys.DELETE_STAFF_SUCCESS;
}

export interface IDeleteStaffFail extends Action {
	readonly type: Keys.DELETE_STAFF_FAIL;
	payload?: string;
}
//#endregion

//#region GET Orders IActions
export interface IGetOrders extends Action {
	readonly type: Keys.GET_ORDERS;
	payload: {
		shopId: string;
		start: number;
		limit: number;
		order?: string;
	};
}
export interface IGetOrdersSuccess extends Action {
	readonly type: Keys.GET_ORDERS_SUCCESS;
	payload: {
		count: number;
		data: IOrderRecord[];
	};
}
export interface IGetOrdersFail extends Action {
	readonly type: Keys.GET_ORDERS_FAIL;
	payload?: string;
}
//#endregion

//#region GET Orders Multiple Shop IActions
export interface IGetOrdersMultipleShop extends Action {
	readonly type: Keys.GET_ORDERS_MULTIPLE_SHOP;
	payload: {
		start: number;
		type: ORDER_TAB;
		sort: number;
		sortCol: 'last_updated' | 'order_time' | string;
		limit: number;
		order?: string;
		listShop?: string[];
	};
}
export interface IGetOrdersMultipleShopSuccess extends Action {
	readonly type: Keys.GET_ORDERS_MULTIPLE_SHOP_SUCCESS;
	payload: {
		count: number;
		data: IOrderRecord[];
	};
}
export interface IGetOrdersMultipleShopFail extends Action {
	readonly type: Keys.GET_ORDERS_MULTIPLE_SHOP_FAIL;
	payload?: string;
}
//#endregion

//#region GET Delivery Countries IActions
export interface IGetDeliveryCountries extends Action {
	readonly type: Keys.GET_DELIVERY_COUNTRIES;
	payload: {
		shopId: string;
	};
}
export interface IGetDeliveryCountriesSuccess extends Action {
	readonly type: Keys.GET_DELIVERY_COUNTRIES_SUCCESS;
	payload: {
		countries: string[];
	};
}
export interface IGetDeliveryCountriesFail extends Action {
	readonly type: Keys.GET_DELIVERY_COUNTRIES_FAIL;
	payload?: string;
}
//#endregion

//#region GET Action Required Orders IActions
export interface IGetActionRequiredOrders extends Action {
	readonly type: Keys.GET_ACTION_REQUIRED_ORDERS;
	payload: {
		shopId: string;
		order?: string;
		start: number;
		limit: number;
	};
}
export interface IGetActionRequiredOrdersSuccess extends Action {
	readonly type: Keys.GET_ACTION_REQUIRED_ORDERS_SUCCESS;
	payload: {
		count: number;
		data: IOrderRecord[];
	};
}
export interface IGetActionRequiredOrdersFail extends Action {
	readonly type: Keys.GET_ACTION_REQUIRED_ORDERS_FAIL;
	payload?: string;
}
//#endregion

//#region POST Full Fill Orders IActions
export interface IPostFullFillOrder extends Action {
	readonly type: Keys.POST_FULL_FILL_ORDERS;
	payload: {
		shopId: string;
		order: IOrder;
	};
}
export interface IPostFullFillOrderSuccess extends Action {
	readonly type: Keys.POST_FULL_FILL_ORDERS_SUCCESS;
	payload: IOrder;
}
export interface IPostFullFillOrderFail extends Action {
	readonly type: Keys.POST_FULL_FILL_ORDERS_FAIL;
	payload?: string;
}
//#endregion

//#region POST Refund Order IActions
export interface IPostRefundOrder extends Action {
	readonly type: Keys.POST_REFUND_ORDER;
	payload: {
		shopId: string;
		orderId: string;
		reasonCode: number;
	};
}
export interface IPostRefundOrderSuccess extends Action {
	readonly type: Keys.POST_REFUND_ORDER_SUCCESS;
	payload: any;
}
export interface IPostRefundOrderFail extends Action {
	readonly type: Keys.POST_REFUND_ORDER_FAIL;
	payload?: string;
}
//#endregion

//#region POST Modify Shipped Order IActions
export interface IPostModifyShippedOrder extends Action {
	readonly type: Keys.POST_MODIFY_SHIPPED_ORDER;
	payload: {
		shopId: string;
		order: IOrder;
	};
}
export interface IPostModifyShippedOrderSuccess extends Action {
	readonly type: Keys.POST_MODIFY_SHIPPED_ORDER_SUCCESS;
	payload: any;
}
export interface IPostModifyShippedOrderFail extends Action {
	readonly type: Keys.POST_MODIFY_SHIPPED_ORDER_FAIL;
	payload?: string;
}
//#endregion

//#region GET Shipping Carrier By Locale IActions
export interface IGetShippingCarrierByLocale extends Action {
	readonly type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE;
	payload: {
		shopId: string;
		locale: string;
	};
}
export interface IGetShippingCarrierByLocaleSuccess extends Action {
	readonly type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE_SUCCESS;
	payload: {
		shipping_carriers: string[];
	};
}
export interface IGetShippingCarrierByLocaleFail extends Action {
	readonly type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE_FAIL;
	payload?: string;
}
//#endregion

//#region GET Note Orders IActions
export interface IGetNoteOrders extends Action {
	readonly type: Keys.GET_NOTE_ORDERS;
	payload: {
		shopId: string;
		start: number;
		limit: number;
		order?: string;
	};
}
export interface IGetNoteOrdersSuccess extends Action {
	readonly type: Keys.GET_NOTE_ORDERS_SUCCESS;
	payload: {
		count: number;
		data: IOrderRecord[];
	};
}
export interface IGetNoteOrdersFail extends Action {
	readonly type: Keys.GET_NOTE_ORDERS_FAIL;
	payload?: string;
}
//#endregion

//#region PUT Note Order IActions
export interface IPutNoteOrder extends Action {
	readonly type: Keys.PUT_NOTE_ORDER;
	payload: {
		shopId: string;
		orderId: string;
		isNoted: boolean;
	};
}
export interface IPutNoteOrderSuccess extends Action {
	readonly type: Keys.PUT_NOTE_ORDER_SUCCESS;
	payload: {
		count: number;
		data: IOrderRecord[];
	};
}
export interface IPutNoteOrderFail extends Action {
	readonly type: Keys.PUT_NOTE_ORDER_FAIL;
	payload?: string;
}
//#endregion

//#region GET Original Product Image IActions
export interface IGetOriginalProductImage extends Action {
	readonly type: Keys.GET_ORIGINAL_PRODUCT_IMAGE;
	payload: {
		shopId: string;
		productId: string;
	};
}
export interface IGetOriginalProductImageSuccess extends Action {
	readonly type: Keys.GET_ORIGINAL_PRODUCT_IMAGE_SUCCESS;
	payload: {
		image_url: string;
	};
}
export interface IGetOriginalProductImageFail extends Action {
	readonly type: Keys.GET_ORIGINAL_PRODUCT_IMAGE_FAIL;
	payload?: string;
}
//#endregion
