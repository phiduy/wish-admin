import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import {
	IShopInfo,
	IOrder,
	IShopStaff,
	IOrderRecord,
	IShopRecord,
	SHOP_MODAL,
	SHOP_VIEW,
	SHOP_CLEAR,
	ORDER_TAB,
} from './model/IShopState';

export const handleClear = (data: { type: SHOP_CLEAR }): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: {
			type: data.type,
		},
	};
};

export const handleCurrentShop = (data: {
	shopInfo: IShopRecord;
	type: 'update' | 'delete' | 'detail' | 'sync';
}): IActions.IHandleCurrentShop => {
	return {
		type: Keys.HANDLE_CURRENT_SHOP,
		payload: {
			shopInfo: data.shopInfo,
			type: data.type,
		},
	};
};

export const handleCurrentOrder = (data: {
	order: IOrderRecord;
	type: 'ship' | 'detail';
}): IActions.IHandleCurrentOrder => {
	return {
		type: Keys.HANDLE_CURRENT_ORDER,
		payload: {
			order: data.order,
			type: data.type,
		},
	};
};

export const handleCurrentStaff = (data: {
	staff: IShopStaff;
	type: 'update' | 'delete' | 'detail';
}): IActions.IHandleCurrentStaff => {
	return {
		type: Keys.HANDLE_CURRENT_STAFF,
		payload: {
			staff: data.staff,
			type: data.type,
		},
	};
};

export const toggleView = (data: { type: SHOP_VIEW }): IActions.IToggleView => {
	return {
		type: Keys.TOGGLE_VIEW,
		payload: {
			type: data.type,
		},
	};
};

export const toggleModal = (data: { type: SHOP_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type: data.type,
		},
	};
};

//#region GET Shop Actions
export const getShops = (data: { page: number; limit?: number }): IActions.IGetShops => {
	return {
		type: Keys.GET_SHOPS,
		payload: data,
	};
};

export const getShopsSuccess = (res: {
	current_page: number;
	items: IShopRecord[];
	per_page: number;
	total_item: number;
	total_page: number;
}): IActions.IGetShopsSuccess => {
	return {
		type: Keys.GET_SHOPS_SUCCESS,
		payload: res,
	};
};

export const getShopsFail = (res: any): IActions.IGetShopsFail => {
	return {
		type: Keys.GET_SHOPS_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Sync Data Actions
export const getSyncData = (data: { shopId: string; date: string }): IActions.IGetSyncData => {
	return {
		type: Keys.GET_SYNC_DATA,
		payload: {
			shopId: data.shopId,
			date: data.date,
		},
	};
};

export const getSyncDataSuccess = (res: {
	current_page: number;
	items: IShopRecord[];
	per_page: number;
	total_item: number;
	total_page: number;
}): IActions.IGetSyncDataSuccess => {
	return {
		type: Keys.GET_SYNC_DATA_SUCCESS,
		payload: res,
	};
};

export const getSyncDataFail = (res: any): IActions.IGetSyncDataFail => {
	return {
		type: Keys.GET_SYNC_DATA_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Shop Detail Actions
export const getShopDetail = (data: { shopId: string }): IActions.IGetShopDetail => {
	return {
		type: Keys.GET_SHOP_DETAIL,
		payload: {
			shopId: data.shopId,
		},
	};
};

export const getShopDetailSuccess = (res: IShopRecord): IActions.IGetShopDetailSuccess => {
	return {
		type: Keys.GET_SHOP_DETAIL_SUCCESS,
		payload: res,
	};
};

export const getShopDetailFail = (res: any): IActions.IGetShopDetailFail => {
	return {
		type: Keys.GET_SHOP_DETAIL_FAIL,
		payload: res,
	};
};
//#endregion

//#region CREATE Shop Actions
export const createShop = (data: IShopInfo): IActions.ICreateShop => {
	return {
		type: Keys.CREATE_SHOP,
		payload: data,
	};
};

export const createShopSuccess = (res: any): IActions.ICreateShopSuccess => {
	return {
		type: Keys.CREATE_SHOP_SUCCESS,
		payload: res,
	};
};

export const createShopFail = (res: any): IActions.ICreateShopFail => {
	return {
		type: Keys.CREATE_SHOP_FAIL,
		payload: res,
	};
};
//#endregion

//#region UPDATE Shop Actions
export const updateShop = (data: { shopInfo: IShopInfo; shopId: string }): IActions.IUpdateShop => {
	return {
		type: Keys.UPDATE_SHOP,
		payload: data,
	};
};

export const updateShopSuccess = (res: any): IActions.IUpdateShopSuccess => {
	return {
		type: Keys.UPDATE_SHOP_SUCCESS,
		payload: res,
	};
};

export const updateShopFail = (res: any): IActions.IUpdateShopFail => {
	return {
		type: Keys.UPDATE_SHOP_FAIL,
		payload: res,
	};
};
//#endregion

//#region DELETE Shop Actions
export const deleteShop = (data: { shopId: string }): IActions.IDeleteShop => {
	return {
		type: Keys.DELETE_SHOP,
		payload: data,
	};
};

export const deleteShopSuccess = (): IActions.IDeleteShopSuccess => {
	return {
		type: Keys.DELETE_SHOP_SUCCESS,
	};
};

export const deleteShopFail = (res: any): IActions.IDeleteShopFail => {
	return {
		type: Keys.DELETE_SHOP_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Code Url Actions
export const getCodeUrl = (data: { shopId: string }): IActions.IGetCodeUrl => {
	return {
		type: Keys.GET_CODE_URL,
		payload: {
			shopId: data.shopId,
		},
	};
};

export const getCodeUrlSuccess = (res: { url: string }): IActions.IGetCodeUrlSuccess => {
	return {
		type: Keys.GET_CODE_URL_SUCCESS,
		payload: res,
	};
};

export const getCodeUrlFail = (res: any): IActions.IGetCodeUrlFail => {
	return {
		type: Keys.GET_CODE_URL_FAIL,
		payload: res,
	};
};
//#endregion

//#region Add Code Actions
export const addCode = (data: { shopId: string; code: string }): IActions.IAddCode => {
	return {
		type: Keys.ADD_CODE,
		payload: {
			shopId: data.shopId,
			code: data.code,
		},
	};
};

export const addCodeSuccess = (res: string): IActions.IAddCodeSuccess => {
	return {
		type: Keys.ADD_CODE_SUCCESS,
		payload: res,
	};
};

export const addCodeFail = (res: any): IActions.IAddCodeFail => {
	return {
		type: Keys.ADD_CODE_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Refresh Token Url Actions
export const getRefreshTokenUrl = (data: { shopId: string }): IActions.IGetRefreshTokenUrl => {
	return {
		type: Keys.GET_REFRESH_TOKEN_URL,
		payload: {
			shopId: data.shopId,
		},
	};
};

export const getRefreshTokenUrlSuccess = (res: {
	url: string;
}): IActions.IGetRefreshTokenUrlSuccess => {
	return {
		type: Keys.GET_REFRESH_TOKEN_URL_SUCCESS,
		payload: res,
	};
};

export const getRefreshTokenUrlFail = (res: any): IActions.IGetRefreshTokenUrlFail => {
	return {
		type: Keys.GET_REFRESH_TOKEN_URL_FAIL,
		payload: res,
	};
};
//#endregion

//#region Update Access Token Actions
export const updateAccessToken = (data: {
	shopId: string;
	accessToken: string;
	expiredTime: string;
}): IActions.IUpdateAccessToken => {
	return {
		type: Keys.UPDATE_ACCESS_TOKEN,
		payload: {
			...data,
		},
	};
};

export const updateAccessTokenSuccess = (): IActions.IUpdateAccessTokenSuccess => {
	return {
		type: Keys.UPDATE_ACCESS_TOKEN_SUCCESS,
	};
};

export const updateAccessTokenFail = (res: any): IActions.IUpdateAccessTokenFail => {
	return {
		type: Keys.UPDATE_ACCESS_TOKEN_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Staff Actions
export const getStaff = (data: { shopId: string }): IActions.IGetStaff => {
	return {
		type: Keys.GET_STAFF,
		payload: {
			shopId: data.shopId,
		},
	};
};

export const getStaffSuccess = (res: IShopStaff[]): IActions.IGetStaffSuccess => {
	return {
		type: Keys.GET_STAFF_SUCCESS,
		payload: res,
	};
};

export const getStaffFail = (res: any): IActions.IGetStaffFail => {
	return {
		type: Keys.GET_STAFF_FAIL,
		payload: res,
	};
};
//#endregion

//#region CREATE Staff Actions
export const createStaff = (data: { staff: IShopStaff; shopId: string }): IActions.ICreateStaff => {
	return {
		type: Keys.CREATE_STAFF,
		payload: data,
	};
};

export const createStaffSuccess = (res: any): IActions.ICreateStaffSuccess => {
	return {
		type: Keys.CREATE_STAFF_SUCCESS,
		payload: res,
	};
};

export const createStaffFail = (res: any): IActions.ICreateStaffFail => {
	return {
		type: Keys.CREATE_STAFF_FAIL,
		payload: res,
	};
};
//#endregion

//#region DELETE Staff Actions
export const deleteStaff = (data: { staffId: string; shopId: string }): IActions.IDeleteStaff => {
	return {
		type: Keys.DELETE_STAFF,
		payload: data,
	};
};

export const deleteStaffSuccess = (): IActions.IDeleteStaffSuccess => {
	return {
		type: Keys.DELETE_STAFF_SUCCESS,
	};
};

export const deleteStaffFail = (res: any): IActions.IDeleteStaffFail => {
	return {
		type: Keys.DELETE_STAFF_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Orders Actions
export const getOrders = (data: {
	shopId: string;
	order?: string;
	limit: number;
	start: number;
}): IActions.IGetOrders => {
	return {
		type: Keys.GET_ORDERS,
		payload: data,
	};
};

export const getOrdersSuccess = (res: {
	count: number;
	data: IOrderRecord[];
}): IActions.IGetOrdersSuccess => {
	return {
		type: Keys.GET_ORDERS_SUCCESS,
		payload: res,
	};
};

export const getOrdersFail = (res: any): IActions.IGetOrdersFail => {
	return {
		type: Keys.GET_ORDERS_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Orders Multiple Shiop Actions
export const getOrdersMultipleShop = (data: {
	start: number;
	type: ORDER_TAB;
	sort: number;
	limit: number;
	order?: string;
	sortCol: 'last_updated' | 'order_time' | string;
	listShop?: string[];
}): IActions.IGetOrdersMultipleShop => {
	return {
		type: Keys.GET_ORDERS_MULTIPLE_SHOP,
		payload: data,
	};
};

export const getOrdersMultipleShopSuccess = (res: {
	count: number;
	data: IOrderRecord[];
}): IActions.IGetOrdersMultipleShopSuccess => {
	return {
		type: Keys.GET_ORDERS_MULTIPLE_SHOP_SUCCESS,
		payload: res,
	};
};

export const getOrdersMultipleShopFail = (res: any): IActions.IGetOrdersMultipleShopFail => {
	return {
		type: Keys.GET_ORDERS_MULTIPLE_SHOP_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Note Orders Actions
export const getNoteOrders = (data: {
	shopId: string;
	order?: string;
	limit: number;
	start: number;
}): IActions.IGetNoteOrders => {
	return {
		type: Keys.GET_NOTE_ORDERS,
		payload: data,
	};
};

export const getNoteOrdersSuccess = (res: {
	count: number;
	data: IOrderRecord[];
}): IActions.IGetNoteOrdersSuccess => {
	return {
		type: Keys.GET_NOTE_ORDERS_SUCCESS,
		payload: res,
	};
};

export const getNoteOrdersFail = (res: any): IActions.IGetNoteOrdersFail => {
	return {
		type: Keys.GET_NOTE_ORDERS_FAIL,
		payload: res,
	};
};
//#endregion

//#region PUT Note Order Actions
export const putNoteOrder = (data: {
	shopId: string;
	orderId: string;
	isNoted: boolean;
}): IActions.IPutNoteOrder => {
	return {
		type: Keys.PUT_NOTE_ORDER,
		payload: data,
	};
};

export const putNoteOrderSuccess = (res: {
	count: number;
	data: IOrderRecord[];
}): IActions.IPutNoteOrderSuccess => {
	return {
		type: Keys.PUT_NOTE_ORDER_SUCCESS,
		payload: res,
	};
};

export const putNoteOrderFail = (res: any): IActions.IPutNoteOrderFail => {
	return {
		type: Keys.PUT_NOTE_ORDER_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Action Required Orders Actions
export const getActionRequiredOrders = (data: {
	shopId: string;
	order?: string;
	limit: number;
	start: number;
}): IActions.IGetActionRequiredOrders => {
	return {
		type: Keys.GET_ACTION_REQUIRED_ORDERS,
		payload: data,
	};
};

export const getActionRequiredOrdersSuccess = (res: {
	count: number;
	data: IOrderRecord[];
}): IActions.IGetActionRequiredOrdersSuccess => {
	return {
		type: Keys.GET_ACTION_REQUIRED_ORDERS_SUCCESS,
		payload: res,
	};
};

export const getActionRequiredOrdersFail = (res: any): IActions.IGetActionRequiredOrdersFail => {
	return {
		type: Keys.GET_ACTION_REQUIRED_ORDERS_FAIL,
		payload: res,
	};
};
//#endregion

//#region POST Full Fill Order Actions
export const postFullFillOrder = (data: {
	shopId: string;
	order: IOrder;
}): IActions.IPostFullFillOrder => {
	return {
		type: Keys.POST_FULL_FILL_ORDERS,
		payload: data,
	};
};

export const postFullFillOrderSuccess = (res: IOrder): IActions.IPostFullFillOrderSuccess => {
	return {
		type: Keys.POST_FULL_FILL_ORDERS_SUCCESS,
		payload: res,
	};
};

export const postFullFillOrderFail = (res: any): IActions.IPostFullFillOrderFail => {
	return {
		type: Keys.POST_FULL_FILL_ORDERS_FAIL,
		payload: res,
	};
};
//#endregion

//#region POST Refund Order Actions
export const postRefundOrder = (data: {
	shopId: string;
	orderId: string;
	reasonCode: number;
}): IActions.IPostRefundOrder => {
	return {
		type: Keys.POST_REFUND_ORDER,
		payload: data,
	};
};

export const postRefundOrderSuccess = (res: IOrder): IActions.IPostRefundOrderSuccess => {
	return {
		type: Keys.POST_REFUND_ORDER_SUCCESS,
		payload: res,
	};
};

export const postRefundOrderFail = (res: any): IActions.IPostRefundOrderFail => {
	return {
		type: Keys.POST_REFUND_ORDER_FAIL,
		payload: res,
	};
};
//#endregion

//#region POST Modify Shipped Order Actions
export const postModifyShippedOrder = (data: {
	shopId: string;
	order: IOrder;
}): IActions.IPostModifyShippedOrder => {
	return {
		type: Keys.POST_MODIFY_SHIPPED_ORDER,
		payload: data,
	};
};

export const postModifyShippedOrderSuccess = (
	res: IOrder
): IActions.IPostModifyShippedOrderSuccess => {
	return {
		type: Keys.POST_MODIFY_SHIPPED_ORDER_SUCCESS,
		payload: res,
	};
};

export const postModifyShippedOrderFail = (res: any): IActions.IPostModifyShippedOrderFail => {
	return {
		type: Keys.POST_MODIFY_SHIPPED_ORDER_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Delivery Countries Actions
export const getDeliveryCountries = (data: { shopId: string }): IActions.IGetDeliveryCountries => {
	return {
		type: Keys.GET_DELIVERY_COUNTRIES,
		payload: data,
	};
};

export const getDeliveryCountriesSuccess = (res: {
	countries: string[];
}): IActions.IGetDeliveryCountriesSuccess => {
	return {
		type: Keys.GET_DELIVERY_COUNTRIES_SUCCESS,
		payload: res,
	};
};

export const getDeliveryCountriesFail = (res: any): IActions.IGetDeliveryCountriesFail => {
	return {
		type: Keys.GET_DELIVERY_COUNTRIES_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Delivery Countries Actions
export const getShippingCarrierByLocale = (data: {
	shopId: string;
	locale: string;
}): IActions.IGetShippingCarrierByLocale => {
	return {
		type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE,
		payload: data,
	};
};

export const getShippingCarrierByLocaleSuccess = (res: {
	shipping_carriers: string[];
}): IActions.IGetShippingCarrierByLocaleSuccess => {
	return {
		type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE_SUCCESS,
		payload: res,
	};
};

export const getShippingCarrierByLocaleFail = (
	res: any
): IActions.IGetShippingCarrierByLocaleFail => {
	return {
		type: Keys.GET_SHIPPING_CARRIER_BY_LOCALE_FAIL,
		payload: res,
	};
};
//#endregion

//#region GET Get Original Product Image Actions
export const getOriginalProductImage = (data: {
	shopId: string;
	productId: string;
}): IActions.IGetOriginalProductImage => {
	return {
		type: Keys.GET_ORIGINAL_PRODUCT_IMAGE,
		payload: data,
	};
};

export const getOriginalProductImageSuccess = (res: {
	image_url: string;
}): IActions.IGetOriginalProductImageSuccess => {
	return {
		type: Keys.GET_ORIGINAL_PRODUCT_IMAGE_SUCCESS,
		payload: res,
	};
};

export const getOriginalProductImageFail = (res: any): IActions.IGetOriginalProductImageFail => {
	return {
		type: Keys.GET_ORIGINAL_PRODUCT_IMAGE_FAIL,
		payload: res,
	};
};
//#endregion
