import { push } from 'connected-react-router';
import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as ShopApi from '../../../api/shop';
import * as OrderApi from '../../../api/order';
import { message } from 'antd';

// Handle Get Shops
function* handleGetShops(action: any) {
	try {
		const res = yield call(ShopApi.getShops, action.payload);
		if (res.status === 200) {
			yield put(actions.getShopsSuccess(res.data));
		} else {
			yield put(actions.getShopsFail(res));
		}
	} catch (error) {
		yield put(actions.getShopsFail(error));
	}
}

// Handle Get Sync Data
function* handleGetSyncData(action: any) {
	try {
		const res = yield call(ShopApi.getSyncData, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Sync Data Successful', 2);
			yield put(actions.getSyncDataSuccess(res.data));
		} else {
			yield put(actions.getSyncDataFail(res));
		}
	} catch (error) {
		yield put(actions.getSyncDataFail(error));
	}
}

// Handle Get Shop Detail
function* handleGetShopDetail(action: any) {
	try {
		const res = yield call(ShopApi.getShopDetail, action.payload);
		if (res.status === 200) {
			yield put(actions.getShopDetailSuccess(res.data));
		} else {
			yield put(actions.getShopDetailFail(res));
		}
	} catch (error) {
		yield put(actions.getShopDetailFail(error));
	}
}

// Handle Create Shop
function* handleCreateShop(action: any) {
	try {
		const res = yield call(ShopApi.createShop, action.payload);
		if (res.status === 201 || res.status === 200) {
			message.success('Create Shop Successful', 2);
			yield put(actions.createShopSuccess(res.data));
			yield put(actions.getShops({ page: 0 }));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.createShopFail(res));
		}
	} catch (error) {
		yield put(actions.createShopFail(error));
	}
}

// Handle Update Shop
function* handleUpdateShop(action: any) {
	try {
		const res = yield call(ShopApi.updateShop, action.payload);
		if (res.status === 200) {
			message.success('Update Shop Successful', 2);
			yield put(actions.updateShopSuccess(res.data));
		} else {
			yield put(actions.updateShopFail(res));
		}
	} catch (error) {
		yield put(actions.updateShopFail(error));
	}
}

// Handle Delete Shop
function* handleDeleteShop(action: any) {
	try {
		const res = yield call(ShopApi.deleteShop, action.payload);
		yield delay(200);
		if (res.status === 204) {
			message.success('Delete Shop Successful', 2);
			yield put(actions.deleteShopSuccess());
			// yield put(actions.getShops());
		} else {
			yield put(actions.deleteShopFail(res));
		}
	} catch (error) {
		yield put(actions.deleteShopFail(error));
	}
}

// Handle Get Code Url
function* handleGetCodeUrl(action: any) {
	try {
		const res = yield call(ShopApi.getCodeUrl, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Get Code Successful', 2);
			yield put(actions.getCodeUrlSuccess(res.data));
		} else {
			yield put(actions.getCodeUrlFail(res));
		}
	} catch (error) {
		yield put(actions.getCodeUrlFail(error));
	}
}

// Handle Add Code Url
function* handleAddCode(action: any) {
	try {
		const res = yield call(ShopApi.addCode, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Add code successful', 2);
			yield put(actions.addCodeSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.addCodeFail(res));
		}
	} catch (error) {
		yield put(actions.addCodeFail(error));
	}
}

// Handle Get Refresh Token Url
function* handleGetRefreshTokenUrl(action: any) {
	try {
		const res = yield call(ShopApi.getShopRefreshTokenUrl, action.payload);
		yield delay(200);
		if (res.status === 200) {
			message.success('Refresh Token Successful', 2);
			yield put(actions.getRefreshTokenUrlSuccess(res.data));
		} else {
			message.error('Wish Server Error. Try again after few minute', 2);
			yield put(actions.getRefreshTokenUrlFail(res));
		}
	} catch (error) {
		yield put(actions.getRefreshTokenUrlFail(error));
	}
}

// Handle Update Access Token
function* handleUpdateAccessToken(action: any) {
	try {
		const res = yield call(ShopApi.updateAccessToken, action.payload);
		yield delay(200);
		if (res.status === 200) {
			yield put(actions.updateAccessTokenSuccess());
		} else {
			yield put(actions.updateAccessTokenFail(res));
		}
	} catch (error) {
		yield put(actions.updateAccessTokenFail(error));
	}
}

// Handle Get Staff
function* handleGetStaff(action: any) {
	try {
		const res = yield call(ShopApi.getShopStaff, action.payload);
		yield delay(200);
		if (res.status === 200) {
			yield put(actions.getStaffSuccess(res.data));
		} else {
			yield put(actions.getStaffFail(res));
		}
	} catch (error) {
		yield put(actions.getStaffFail(error));
	}
}

// Handle Create Staff
function* handleCreateStaff(action: any) {
	try {
		const res = yield call(ShopApi.addShopStaff, action.payload);
		yield delay(200);
		if (res.status === 201 || res.status === 200) {
			message.success('Create shop successful', 2);
			yield put(actions.createStaffSuccess(res.data));
		} else {
			message.error('Email is not exist', 2);
			yield put(actions.createStaffFail(res.data.error));
		}
	} catch (error) {
		yield put(actions.createStaffFail(error));
	}
}

// Handle Delete Staff
function* handleDeleteStaff(action: any) {
	try {
		const res = yield call(ShopApi.deleteShopStaff, action.payload);
		yield delay(200);
		if (res.status === 200 || res.status === 204) {
			message.success('Delete Staff Successful', 2);
			yield put(actions.deleteStaffSuccess());
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.deleteStaffFail(res.data.error));
		}
	} catch (error) {
		yield put(actions.deleteStaffFail(error));
	}
}

// Handle Get Orders
function* handleGetOrders(action: any) {
	try {
		const res = yield call(OrderApi.getOrders, action.payload);
		if (res.status === 200) {
			yield put(actions.getOrdersSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.getOrdersFail(res.data));
		}
	} catch (error) {
		yield put(actions.getOrdersFail(error));
	}
}

// Handle Get Orders Multiple Shop
function* handleGetOrdersMultipleShop(action: any) {
	try {
		const res = yield call(OrderApi.getOrdersMultipleShop, action.payload);
		if (res.status === 200) {
			yield put(actions.getOrdersMultipleShopSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			switch (res.status) {
				case 400:
					yield put(push('/shop/list'));
					break;
				default:
					break;
			}
			yield put(actions.getOrdersMultipleShopFail(res.data));
		}
	} catch (error) {
		yield put(actions.getOrdersMultipleShopFail(error));
	}
}

// Handle Get Note Orders
function* handleGetNoteOrders(action: any) {
	try {
		const res = yield call(OrderApi.getNoteOrders, action.payload);
		if (res.status === 200) {
			yield put(actions.getNoteOrdersSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.getNoteOrdersFail(res.data));
		}
	} catch (error) {
		yield put(actions.getNoteOrdersFail(error));
	}
}

// Handle Put Note Order
function* handlePutNoteOrder(action: any) {
	try {
		const res = yield call(OrderApi.putNoteOrder, action.payload);
		if (res.status === 200) {
			message.success('Change Note Order Successful', 2);
			yield put(actions.putNoteOrderSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.putNoteOrderFail(res.data));
		}
	} catch (error) {
		yield put(actions.putNoteOrderFail(error));
	}
}

// Handle Get Full Fill Order
function* handleGetActionRequiredOrder(action: any) {
	try {
		const res = yield call(OrderApi.getActionRequiredOrder, action.payload);
		if (res.status === 200) {
			yield put(actions.getActionRequiredOrdersSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.getActionRequiredOrdersFail(res));
		}
	} catch (error) {
		yield put(actions.getActionRequiredOrdersFail(error));
	}
}

// Handle Post Full Fill Order
function* handlePostFullFillOrder(action: any) {
	try {
		const res = yield call(OrderApi.postFullFillOrder, action.payload);
		switch (res.status) {
			case 200:
				message.success('Ship Order Successful', 2);
				yield put(actions.postFullFillOrderSuccess(res.data));
				break;
			default:
				message.error(res.data.error.message, 3);
				throw new Error(res.data.error.message);
		}
	} catch (error) {
		yield put(actions.postFullFillOrderFail(error));
	}
}

// Handle Post Refund Order
function* handlePostRefundOrder(action: any) {
	try {
		const res = yield call(OrderApi.postRefundOrder, action.payload);
		switch (res.status) {
			case 200:
				message.success('Refund Is In Progress', 2);
				yield put(actions.postRefundOrderSuccess(res.data));
				break;
			case 501:
				message.error(res.data.error.message, 3);
				throw new Error(res.data.error.message);
			default:
				const { details } = res.data;
				if (details !== undefined) {
					message.error(details[0].message, 3);
					throw new Error(details[0].message);
				}
				message.error('Server Error', 3);
				throw new Error('Server Error');
		}
	} catch (error) {
		yield put(actions.postRefundOrderFail(error));
	}
}

// Handle Post Modify Shipped Order
function* handlePostModifyShippedOrder(action: any) {
	try {
		const res = yield call(OrderApi.postModifyShippedOrder, action.payload);
		switch (res.status) {
			case 200:
				message.success('Modify Tracking Order Successful', 2);
				yield put(actions.postModifyShippedOrderSuccess(res.data));
				break;
			case 501:
				message.error(res.data.error.message, 3);
				throw new Error(res.data.error.message);
			default:
				const { details } = res.data;
				if (details !== undefined) {
					message.error(details[0].message, 3);
					throw new Error(details[0].message);
				}
				message.error('Server Error', 3);
				throw new Error('Server Error');
		}
	} catch (error) {
		yield put(actions.postModifyShippedOrderFail(error));
	}
}

// Handle Get Shipping Carrier
function* handleGetShippingCarrierByLocale(action: any) {
	try {
		const res = yield call(OrderApi.getShippingCarrierByLocale, action.payload);
		if (res.status === 200) {
			yield put(actions.getShippingCarrierByLocaleSuccess(res.data));
		} else {
			yield put(actions.getShippingCarrierByLocaleFail(res));
		}
	} catch (error) {
		yield put(actions.getShippingCarrierByLocaleFail(error));
	}
}

// Handle Get Delivery Countries
function* handleGetDeliveryCountries(action: any) {
	try {
		const res = yield call(OrderApi.getDeliveryCountries, action.payload);
		if (res.status === 200) {
			yield put(actions.getDeliveryCountriesSuccess(res.data));
		} else {
			yield put(actions.getDeliveryCountriesFail(res));
		}
	} catch (error) {
		yield put(actions.getDeliveryCountriesFail(error));
	}
}

// Handle Get Original Product Image
function* handleGetOriginalProductImage(action: any) {
	try {
		const res = yield call(OrderApi.getOriginalProductImage, action.payload);
		if (res.status === 200) {
			yield put(actions.getOriginalProductImageSuccess(res.data));
		} else {
			const { details } = res.data;
			message.error(details[0].message, 3);
			yield put(actions.getOriginalProductImageFail(res.data));
		}
	} catch (error) {
		yield put(actions.getOriginalProductImageFail(error));
	}
}
/*-----------------------------------------------------------------*/
function* watchGetShops() {
	yield takeEvery(Keys.GET_SHOPS, handleGetShops);
}
function* watchGetSyncData() {
	yield takeEvery(Keys.GET_SYNC_DATA, handleGetSyncData);
}
function* watchCreateShop() {
	yield takeEvery(Keys.CREATE_SHOP, handleCreateShop);
}
function* watchUpdateShop() {
	yield takeEvery(Keys.UPDATE_SHOP, handleUpdateShop);
}
function* watchDeleteShop() {
	yield takeEvery(Keys.DELETE_SHOP, handleDeleteShop);
}
function* watchGetCodeUrl() {
	yield takeEvery(Keys.GET_CODE_URL, handleGetCodeUrl);
}
function* watchAddCode() {
	yield takeEvery(Keys.ADD_CODE, handleAddCode);
}
function* watchGetRefreshTokenUrl() {
	yield takeEvery(Keys.GET_REFRESH_TOKEN_URL, handleGetRefreshTokenUrl);
}
function* watchGetStaff() {
	yield takeEvery(Keys.GET_STAFF, handleGetStaff);
}
function* watchCreateStaff() {
	yield takeEvery(Keys.CREATE_STAFF, handleCreateStaff);
}
function* watchDeleteStaff() {
	yield takeEvery(Keys.DELETE_STAFF, handleDeleteStaff);
}
function* watchGetShopDetail() {
	yield takeEvery(Keys.GET_SHOP_DETAIL, handleGetShopDetail);
}
function* watchGetShippingCarrierByLocale() {
	yield takeEvery(Keys.GET_SHIPPING_CARRIER_BY_LOCALE, handleGetShippingCarrierByLocale);
}
function* watchGetDeliveryCountries() {
	yield takeEvery(Keys.GET_DELIVERY_COUNTRIES, handleGetDeliveryCountries);
}
function* watchGetOrders() {
	yield takeEvery(Keys.GET_ORDERS, handleGetOrders);
}
function* watchGetOrdersMultipleShop() {
	yield takeEvery(Keys.GET_ORDERS_MULTIPLE_SHOP, handleGetOrdersMultipleShop);
}
function* watchGetNoteOrders() {
	yield takeEvery(Keys.GET_NOTE_ORDERS, handleGetNoteOrders);
}
function* watchPutNoteOrder() {
	yield takeEvery(Keys.PUT_NOTE_ORDER, handlePutNoteOrder);
}
function* watchGetActionRequiredOrder() {
	yield takeEvery(Keys.GET_ACTION_REQUIRED_ORDERS, handleGetActionRequiredOrder);
}
function* watchPostFullFillOrder() {
	yield takeEvery(Keys.POST_FULL_FILL_ORDERS, handlePostFullFillOrder);
}
function* watchPostRefundOrder() {
	yield takeEvery(Keys.POST_REFUND_ORDER, handlePostRefundOrder);
}
function* watchPostModifyShippedOrder() {
	yield takeEvery(Keys.POST_MODIFY_SHIPPED_ORDER, handlePostModifyShippedOrder);
}
function* watchUpdateAccessToken() {
	yield takeEvery(Keys.UPDATE_ACCESS_TOKEN, handleUpdateAccessToken);
}
function* watchGetOriginalProductImage() {
	yield takeEvery(Keys.GET_ORIGINAL_PRODUCT_IMAGE, handleGetOriginalProductImage);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchGetShops,
	watchGetSyncData,
	watchGetShopDetail,
	watchCreateShop,
	watchUpdateShop,
	watchDeleteShop,
	watchGetCodeUrl,
	watchAddCode,
	watchGetRefreshTokenUrl,
	watchGetStaff,
	watchCreateStaff,
	watchDeleteStaff,
	watchGetShippingCarrierByLocale,
	watchGetDeliveryCountries,
	watchGetOrders,
	watchGetOrdersMultipleShop,
	watchGetNoteOrders,
	watchPutNoteOrder,
	watchGetActionRequiredOrder,
	watchPostFullFillOrder,
	watchPostRefundOrder,
	watchPostModifyShippedOrder,
	watchUpdateAccessToken,
	watchGetOriginalProductImage,
];

export default sagas;
