import React from 'react';
import { IShopProps } from '../../model/IShopProps';
import { IColumn } from '../../../../common/interfaces';
import {
	Button,
	Tag,
	Table,
	Menu,
	Dropdown,
	Space,
	Tooltip,
	DatePicker,
	Form,
	Popconfirm,
} from 'antd';
import {
	EditOutlined,
	DeleteOutlined,
	FileSearchOutlined,
	ControlOutlined,
	SnippetsOutlined,
	PlusOutlined,
	ReloadOutlined,
	SyncOutlined,
	CheckOutlined,
	CloseOutlined,
	ScheduleOutlined,
	ApiOutlined,
} from '@ant-design/icons';
import { IShopRecord, ORDER_TAB, SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';
import { Link, RouteComponentProps } from 'react-router-dom';
import { DEFAULT_LIMIT } from '../../../../common/constants';
import moment from 'moment';

interface IProps extends IShopProps, RouteComponentProps {}

interface ISyncDataFormProps {
	props: IShopProps;
	record: IShopRecord;
}

const SyncDataForm: React.FC<ISyncDataFormProps> = ({ props, record }) => {
	const { isSyncDataSuccess, currentShop } = props.store.ShopPage;
	const [formInstance] = Form.useForm();
	const [toggleSync, setToggleSync] = React.useState<boolean>(false);
	const [processing, setProcessing] = React.useState<boolean>(false);
	const [syncTime, setSyncTime] = React.useState<string | undefined>(undefined);

	React.useEffect(() => {
		if (isSyncDataSuccess && record._id === currentShop?._id) {
			setProcessing(false);
			setToggleSync(false);
			props.actions.handleClear({ type: SHOP_CLEAR.SYNC_DATA });
			props.actions.handleCurrentShop({
				type: 'sync',
				shopInfo: {
					...record,
					shop: {
						...record.shop,
						lastSync: moment(syncTime).toDate().toISOString(),
						syncStatus: 'SCHEDULE',
					},
				},
			});
			setSyncTime(undefined);
		}
		// eslint-disable-next-line
	}, [isSyncDataSuccess]);

	const renderSyncStatus = (status: string) => {
		switch (status) {
			case 'SCHEDULE':
				return (
					<Tooltip
						placement="top"
						title={`Last sync ${moment(record.shop.lastSync).fromNow()}`}
					>
						<Tag icon={<ScheduleOutlined />} color="#2dbe60">
							Schedule
						</Tag>
					</Tooltip>
				);
			default:
				return (
					<Tag icon={<ApiOutlined />} color="#6c757d">
						Un-Sync
					</Tag>
				);
		}
	};

	const onFinish = (data: { date: string }) => {
		setProcessing(true);
		setSyncTime(data.date);
		props.actions.handleCurrentShop({
			type: 'detail',
			shopInfo: record,
		});
		props.actions.getSyncData({
			shopId: record.shop._id as string,
			date: moment(data.date).format('YYYY-MM-DD'),
		});
	};

	if (processing) {
		return (
			<Tag icon={<SyncOutlined spin={true} />} color="processing">
				Processing
			</Tag>
		);
	}

	if (toggleSync) {
		return (
			<React.Fragment>
				<small>Choose date to sync data</small>
				<Form form={formInstance} layout={'inline'} onFinish={onFinish}>
					<Form.Item
						className="m-0"
						name="date"
						rules={[{ required: true, message: 'Please select date to sync' }]}
					>
						<DatePicker />
					</Form.Item>
					<Button
						type="text"
						htmlType="submit"
						icon={<CheckOutlined className="text-success" />}
					/>
					<Button
						type="text"
						icon={<CloseOutlined className="text-danger" />}
						onClick={() => {
							formInstance.resetFields();
							setToggleSync(false);
							setProcessing(false);
						}}
					/>
				</Form>
			</React.Fragment>
		);
	}

	return (
		<React.Fragment>
			{renderSyncStatus(record.shop.syncStatus)}
			<Tooltip placement="top" title="Sync">
				<Button
					type="text"
					icon={<SyncOutlined className="text-primary" />}
					onClick={() => setToggleSync(true)}
				/>
			</Tooltip>
		</React.Fragment>
	);
};

const columns = (props: IProps): IColumn[] => {
	return [
		{
			title: 'Shop',
			dataIndex: 'name',
			render: (text: string, record: IShopRecord) => <span>{record.shop.name}</span>,
		},
		{
			title: 'Token',
			dataIndex: 'token',
			render: (text: string, record: IShopRecord) => {
				if (record.shop.expiredTime !== undefined) {
					const momentExpiredTime = moment(record.shop.expiredTime);
					const { isRefreshingToken } = props.store.ShopPage;
					return (
						<>
							{momentExpiredTime.isSame(moment(), 'day') ? (
								<Tag color="#dc3545">Expired</Tag>
							) : (
								<>
									{momentExpiredTime.isBefore(moment(), 'day') && (
										<span>1 day left token will expired</span>
									)}
									<Tag color="#2dbe60">Active</Tag>
								</>
							)}
							<Tooltip placement="top" title="Refresh Token">
								<Button
									type="text"
									onClick={() =>
										props.actions.getRefreshTokenUrl({
											shopId: record.shop._id as string,
										})
									}
									icon={
										<ReloadOutlined
											spin={isRefreshingToken}
											className="text-primary"
										/>
									}
								/>
							</Tooltip>
						</>
					);
				}
				return (
					<>
						<Tag color="#6c757d">None</Tag>
					</>
				);
			},
		},
		{
			title: 'Sync Data',
			dataIndex: 'token',
			render: (text: string, record: IShopRecord) => (
				<SyncDataForm props={props} record={record} />
			),
		},
		{
			title: 'Orders',
			dataIndex: 'orders',
			render: (text: string, record: IShopRecord) => (
				<Tooltip
					placement="top"
					title={
						record.shop.expiredTime
							? 'View detail the orders'
							: 'Update the token first'
					}
				>
					<Button
						type="text"
						disabled={record.shop.expiredTime === undefined}
						onClick={() => {
							props.history.push(
								`/shop/orders?shopId=${record.shop._id}&type=${ORDER_TAB.ACTION_REQUIRED}&start=0&limit=${DEFAULT_LIMIT}&sort=-1&sortCol=last_updated`
							);
							props.actions.handleCurrentShop({
								shopInfo: record,
								type: 'detail',
							});
						}}
						icon={
							<FileSearchOutlined
								className={
									record.shop.expiredTime ? 'text-primary' : 'text-secondary'
								}
							/>
						}
					/>
				</Tooltip>
			),
		},
		{
			title: 'Action',
			dataIndex: 'operation',
			render: (text: any, record: IShopRecord) => {
				if (record.role !== 'Admin') {
					return <>Permission Denied</>;
				}
				return (
					<Space align="center">
						<Dropdown
							overlay={
								<Menu>
									<Menu.Item
										onClick={() =>
											props.actions.getCodeUrl({
												shopId: record.shop._id as string,
											})
										}
									>
										<SnippetsOutlined className="text-success" />
										Create Code
									</Menu.Item>
									<Menu.Item
										onClick={() => {
											props.actions.handleCurrentShop({
												type: 'detail',
												shopInfo: record,
											});
											props.actions.toggleModal({
												type: SHOP_MODAL.ADD_CODE,
											});
										}}
									>
										<PlusOutlined className="text-primary" />
										Add Code
									</Menu.Item>
								</Menu>
							}
						>
							<Button
								type="text"
								icon={<ControlOutlined className="text-secondary" />}
							/>
						</Dropdown>
						<Tooltip placement="top" title="Edit and Add Staff">
							<Button
								type="text"
								onClick={() => {
									props.history.push(`/shop/detail/${record.shop._id}`);
									props.actions.handleCurrentShop({
										shopInfo: record,
										type: 'update',
									});
									props.actions.getStaff({ shopId: record.shop._id as string });
								}}
								icon={<EditOutlined className="text-primary" />}
							/>
						</Tooltip>
						<Tooltip placement="top" title="Delete">
							<Popconfirm
								title="Are you sure to delete this shop?"
								cancelText="Cancel"
								okText="Yes"
								onConfirm={() => {
									props.actions.handleCurrentShop({
										shopInfo: record,
										type: 'delete',
									});
									props.actions.deleteShop({
										shopId: record?.shop._id as string,
									});
								}}
							>
								<Button
									type="text"
									icon={<DeleteOutlined className="text-danger" />}
								/>
							</Popconfirm>
						</Tooltip>
					</Space>
				);
			},
		},
	];
};

export const ShopTable: React.FC<IProps> = (props) => {
	const { shopRecords, isLoadingShop, totalRecord, isProcessing } = props.store.ShopPage;
	const [pageSize, setPageSize] = React.useState<number>(10);
	const [listShopId, setListShopId] = React.useState<string[]>([]);
	const rowSelection = {
		onChange: (selectedRowKeys: any, selectedRows: IShopRecord[]) => {
			const listShopId = selectedRows.map((item) => item.shop._id);
			setListShopId(listShopId as string[]);
		},
	};
	const hasSelected = listShopId.length > 0;

	const onPageChange = (page: number, pageSize?: number) => {
		setPageSize(pageSize as number);
		props.actions.getShops({
			page: page - 1,
			limit: pageSize,
		});
	};
	return (
		<React.Fragment>
			<Space className="mb-3">
				<Button type="primary" disabled={!hasSelected}>
					<Link
						to={{
							pathname: '/shop/orders',
							search: `?shopId=${listShopId.toString()}&type=${
								ORDER_TAB.ACTION_REQUIRED
							}&start=0&limit=${DEFAULT_LIMIT}&sort=-1&sortCol=last_updated`,
						}}
					>
						{hasSelected ? `View ${listShopId.length} Shops` : 'View Selected Shops'}
					</Link>
				</Button>
				<Button type="primary" className="btn-success">
					<Link
						to={{
							pathname: `/shop/orders/all`,
							search: `?type=${ORDER_TAB.ACTION_REQUIRED}&start=0&limit=${DEFAULT_LIMIT}&sort=-1`,
						}}
					>
						View All
					</Link>
				</Button>
			</Space>
			<Table
				columns={columns(props)}
				dataSource={shopRecords}
				loading={isLoadingShop || isProcessing}
				pagination={{
					total: totalRecord,
					pageSize,
					onChange: onPageChange,
					showSizeChanger: true,
				}}
				rowSelection={{
					type: 'checkbox',
					...rowSelection,
				}}
			/>
		</React.Fragment>
	);
};
