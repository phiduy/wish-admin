import React from 'react';
import { IShopProps } from '../../model/IShopProps';
import {
	Table,
	Typography,
	Space,
	Dropdown,
	Tag,
	Menu,
	Button,
	Tooltip,
	Spin,
	message,
} from 'antd';
import { IOrderRecord, ORDER_TAB, SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';
import { DownOutlined, PaperClipOutlined } from '@ant-design/icons';
import moment from 'moment';
import { DEFAULT_LIMIT } from '../../../../common/constants';
import { ColumnType } from 'antd/lib/table';

const { Paragraph } = Typography;

interface IProps extends IShopProps {
	onPageChange: (page: number) => void;
	curTab: ORDER_TAB | string;
	start: number;
	listShopId?: string[];
}

interface IComponentProps {
	props: IShopProps;
	record: IOrderRecord;
}

const sortTypes = ['order_time', 'hours_to_fulfill', 'last_updated'];

const ActionWithType: React.FC<IComponentProps> = ({ props, record }) => {
	const renderMenuItem = (order: IOrderRecord) => {
		if (order.confirmed_delivery === 'No' && order.tracking_confirmed === 'False') {
			if (order.state === 'SHIPPED' || order.state === 'REFUNDED') {
				return (
					<>
						<Menu.Item
							onClick={() => {
								props.actions.getShippingCarrierByLocale({
									shopId: order.shopId._id,
									locale: 'US',
								});
								props.actions.getDeliveryCountries({
									shopId: order.shopId._id,
								});
								props.actions.handleCurrentOrder({
									type: 'ship',
									order: order,
								});
							}}
						>
							Modify Tracking Info
						</Menu.Item>
						<Menu.Item
							onClick={() => {
								props.actions.handleCurrentOrder({
									type: 'detail',
									order: order,
								});
								props.actions.toggleModal({ type: SHOP_MODAL.REFUND_ORDER });
							}}
						>
							Refund
						</Menu.Item>
					</>
				);
			}
			return (
				<>
					<Menu.Item
						onClick={() => {
							props.actions.getShippingCarrierByLocale({
								shopId: order.shopId._id,
								locale: 'US',
							});
							props.actions.getDeliveryCountries({
								shopId: order.shopId._id,
							});
							props.actions.handleCurrentOrder({
								type: 'ship',
								order: order,
							});
						}}
					>
						Ship
					</Menu.Item>
					<Menu.Item
						onClick={() => {
							props.actions.handleCurrentOrder({
								type: 'detail',
								order: order,
							});
							props.actions.toggleModal({ type: SHOP_MODAL.REFUND_ORDER });
						}}
					>
						Refund
					</Menu.Item>
				</>
			);
		} else if (order.confirmed_delivery === 'Yes' && order.tracking_confirmed === 'True') {
			return (
				<Menu.Item
					onClick={() => {
						props.actions.handleCurrentOrder({
							type: 'detail',
							order: order,
						});
						props.actions.toggleModal({ type: SHOP_MODAL.REFUND_ORDER });
					}}
				>
					Refund
				</Menu.Item>
			);
		} else {
			if (order.state === 'SHIPPED' || order.state === 'REFUNDED') {
				return (
					<Menu.Item
						onClick={() => {
							props.actions.getShippingCarrierByLocale({
								shopId: order.shopId._id,
								locale: 'US',
							});
							props.actions.getDeliveryCountries({
								shopId: order.shopId._id,
							});
							props.actions.handleCurrentOrder({
								type: 'ship',
								order: order,
							});
						}}
					>
						Modify Tracking Info
					</Menu.Item>
				);
			}
			return (
				<Menu.Item
					onClick={() => {
						props.actions.getShippingCarrierByLocale({
							shopId: order.shopId._id,
							locale: 'US',
						});
						props.actions.getDeliveryCountries({
							shopId: order.shopId._id,
						});
						props.actions.handleCurrentOrder({
							type: 'ship',
							order: order,
						});
					}}
				>
					Ship
				</Menu.Item>
			);
		}
	};

	const menu = (
		<Menu>
			{renderMenuItem(record)}
			<Menu.Item
				onClick={() => {
					props.actions.putNoteOrder({
						shopId: record.shopId._id,
						orderId: record.order_id,
						isNoted: !record.isNoted,
					});
					props.actions.handleCurrentOrder({
						type: 'detail',
						order: record,
					});
				}}
			>
				{record.isNoted ? 'Unnoted' : 'Note'}
			</Menu.Item>
		</Menu>
	);
	return (
		<Space align="center">
			<Dropdown overlay={menu} trigger={['click']} placement="bottomCenter" arrow={true}>
				<Button type="primary" ghost={true}>
					Action <DownOutlined />
				</Button>
			</Dropdown>
		</Space>
	);
};

const ProductImageDownloadable: React.FC<IComponentProps> = ({ props, record }) => {
	const { isGettingProductImageUrl, currentOrder } = props.store.ShopPage;
	const [spin, setSpin] = React.useState(false);

	React.useEffect(() => {
		if (currentOrder === undefined && !isGettingProductImageUrl && spin) {
			setSpin(false);
		}
		// eslint-disable-next-line
	}, [isGettingProductImageUrl]);

	return (
		<Tooltip placement="bottom" title="Click the image to download">
			<Spin spinning={spin}>
				<div
					className="mr-1 cursor-pointer"
					style={{ width: 40 }}
					onClick={() => {
						if (!isGettingProductImageUrl) {
							props.actions.handleCurrentOrder({
								type: 'detail',
								order: record,
							});
							props.actions.getOriginalProductImage({
								productId: record.product_id,
								shopId: record.shopId._id,
							});
							setSpin(true);
							message.destroy();
						} else {
							message.info('Currently downloading. Please wait it done.');
						}
					}}
				>
					<img
						src={record.product_image_url}
						style={{ maxWidth: '100%' }}
						alt={record.product_name}
					/>
				</div>
			</Spin>
		</Tooltip>
	);
};

const columns = (props: IShopProps): ColumnType<IOrderRecord>[] => {
	return [
		{
			title: 'Date',
			dataIndex: 'order_time',
			fixed: 'left',
			sorter: true,
			render: (text: string, record: IOrderRecord) => (
				<>
					{moment.utc(record.order_time, 'YYYY-MM-DD').format('DD-MM-YYYY')}
					<br /> UTC
				</>
			),
		},
		{
			title: 'Order Id',
			dataIndex: 'order_id',
			fixed: 'left',
			width: 250,
			render: (text: string, record: IOrderRecord) => (
				<Paragraph copyable={true} className="text-primary">
					{record.order_id}
				</Paragraph>
			),
		},
		{
			title: 'Shop Name',
			dataIndex: 'shopId',
			width: 220,
			render: (text: string, record: IOrderRecord) => (
				<div className="text-danger font-weight-bold">{record.shopId.name}</div>
			),
		},
		{
			title: 'State',
			dataIndex: 'key',
			width: 120,
			sorter: (a: IOrderRecord, b: IOrderRecord) => a.state.length - b.state.length,
			render: (text: string, record: IOrderRecord) => {
				switch (record.state) {
					case 'SHIPPED':
						return <Tag color="#40a9ff">{record.state}</Tag>;
					case 'APPROVED':
						return <Tag color="#2dbe60">{record.state}</Tag>;
					default:
						return <Tag color="#6c757d">{record.state}</Tag>;
				}
			},
		},
		{
			title: 'Note',
			width: 120,
			dataIndex: 'isNoted',
			sorter: (a: IOrderRecord, b: IOrderRecord) => (a.isNoted ? -1 : 1),
			render: (text: string, record: IOrderRecord) => {
				if (record.isNoted) {
					return (
						<Tag icon={<PaperClipOutlined />} color="blue">
							Note
						</Tag>
					);
				}
				return 'Unnoted';
			},
		},
		{
			title: 'Tracking Confirmed',
			width: 120,
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => {
				return <>{record.tracking_confirmed === 'False' ? 'No' : 'Yes'}</>;
			},
		},
		{
			title: 'Wish Confirmed Delivery',
			width: 120,
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => <>{record.confirmed_delivery}</>,
		},
		{
			title: 'Days to Fullfill',
			width: 120,
			dataIndex: 'hours_to_fulfill',
			sorter: (a: IOrderRecord, b: IOrderRecord) =>
				parseInt(a.hours_to_fulfill, 10) - parseInt(b.hours_to_fulfill, 10),
			render: (text: string, record: IOrderRecord) => {
				const daysToFulFIll = parseInt(record.days_to_fulfill, 10);
				const renderDaysToFulFillColor = (day: number) => {
					if (day <= 1) {
						return 'error';
					}
					if (day > 1 && day < 3) {
						return 'warning';
					}
					return 'blue';
				};

				if (daysToFulFIll === 0 || !record.days_to_fulfill) {
					return (
						<Tag color="error">
							{record.hours_to_fulfill ? record.hours_to_fulfill : 0} hours
						</Tag>
					);
				}
				return (
					<Tag color={renderDaysToFulFillColor(daysToFulFIll)}>
						{record.days_to_fulfill} calendar day
					</Tag>
				);
			},
		},
		{
			title: 'View Product (SKU)',
			dataIndex: 'key',
			width: 300,
			render: (text: string, record: IOrderRecord) => (
				<div className="d-flex">
					<ProductImageDownloadable props={props} record={record} />
					<div>
						<div
							className="text-primary cursor-pointer"
							onClick={() => {
								props.actions.handleCurrentOrder({
									type: 'detail',
									order: record,
								});
								props.actions.toggleModal({
									type: SHOP_MODAL.PRODUCT_DETAIL,
								});
							}}
						>
							{record.product_id}
						</div>
						<div>({record.sku})</div>
					</div>
				</div>
			),
		},
		{
			title: 'Currency Code',
			width: 100,
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => <>{record.currency_code}</>,
		},
		{
			title: 'Price',
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => <>${record.price}</>,
		},
		{
			title: 'Cost',
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => <>${record.cost}</>,
		},
		{
			title: 'Shipping',
			dataIndex: 'key',
			width: 100,
			render: (text: string, record: IOrderRecord) => <>${record.shipping}</>,
		},
		{
			title: 'Shipping Cost',
			dataIndex: 'key',
			width: 100,
			render: (text: string, record: IOrderRecord) => <>${record.shipping_cost}</>,
		},
		{
			title: 'Quantity',
			dataIndex: 'key',
			width: 100,
			align: 'center',
			render: (text: string, record: IOrderRecord) => <>{record.quantity}</>,
		},
		{
			title: 'Total Cost',
			dataIndex: 'key',
			render: (text: string, record: IOrderRecord) => <>${record.order_total}</>,
		},
		{
			title: 'Warehouse Name (Warehouse Id)',
			width: 160,
			dataIndex: 'key',

			render: (text: string, record: IOrderRecord) => <>N/A</>,
		},
		{
			title: 'Ship to',
			dataIndex: 'key',
			width: 120,
			align: 'left',
			render: (text: string, record: IOrderRecord) => {
				return (
					<>
						<div
							className="text-primary cursor-pointer"
							onClick={() => {
								props.actions.handleCurrentOrder({ type: 'detail', order: record });
								props.actions.toggleModal({ type: SHOP_MODAL.SHIP_ADDRESS });
							}}
						>
							View
						</div>
						<div>
							{record.ShippingDetail.name},{record.ShippingDetail.country}
						</div>
					</>
				);
			},
		},
		{
			title: 'Shipment Details',
			dataIndex: 'key',
			width: 220,
			align: 'left',
			render: (text: string, record: IOrderRecord) => {
				return (
					<>
						{record.tracking_number ? (
							<span className="text-primary">{record.tracking_number}</span>
						) : (
							'N/A'
						)}
					</>
				);
			},
		},
		{
			title: 'Last Updated',
			dataIndex: 'last_updated',
			width: 140,
			sorter: true,
			render: (text: string, record: IOrderRecord) => (
				<>{moment.utc(record.last_updated, 'YYYY-MM-DD').format('DD-MM-YYYY')} UTC</>
			),
		},
		{
			title: 'Action',
			dataIndex: 'operation',
			fixed: 'right',
			align: 'center',
			width: 140,
			render: (text: any, record: IOrderRecord) => (
				<ActionWithType record={record} props={props} />
			),
		},
	];
};

export const OrderTable: React.FC<IProps> = (props) => {
	const {
		orderRecords,
		isProcessing,
		isLoadingOrder,
		isRefundOrderSuccess,
		isShipOrderSuccess,
	} = props.store.ShopPage;
	const { start, curTab, listShopId } = props;

	React.useEffect(() => {
		if (isRefundOrderSuccess || isShipOrderSuccess) {
			props.actions.getOrdersMultipleShop({
				type: curTab as ORDER_TAB,
				sort: -1,
				start: start,
				limit: DEFAULT_LIMIT,
				order: undefined,
				listShop: listShopId,
				sortCol: 'order_time',
			});
			props.actions.handleClear({
				type: SHOP_CLEAR.SHIP_REFUND_ORDER,
			});
		}
		// eslint-disable-next-line
	}, [isRefundOrderSuccess, isShipOrderSuccess]);

	const handleTableChange = (pagination: any, filters: any, sorter: any) => {
		if (sortTypes.includes(sorter.field)) {
			props.actions.getOrdersMultipleShop({
				type: curTab as ORDER_TAB,
				sort: sorter.order === 'ascend' ? 1 : -1,
				start: start,
				limit: DEFAULT_LIMIT,
				order: undefined,
				listShop: listShopId,
				sortCol: sorter.field ? sorter.field : null,
			});
		}
	};

	return (
		<Table
			columns={columns(props)}
			dataSource={orderRecords}
			loading={isLoadingOrder || isProcessing}
			scroll={{
				x: 3030,
				y: 604,
			}}
			onChange={handleTableChange}
			pagination={false}
		/>
	);
};
