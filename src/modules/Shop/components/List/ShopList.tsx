import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { PageHeader, Button, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { IShopProps } from '../../model/IShopProps';
import { SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';
import { ShopTable } from '../Table';
import { CodeForm, ShopForm } from '../Form';

interface IProps extends IShopProps, RouteComponentProps {}

export const ShopList: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.getShops({ page: 0, limit: 10 });
		return () => {
			props.actions.handleClear({ type: SHOP_CLEAR.GET_SHOP });
		};
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<div className="site-content">
				<PageHeader
					title="Shop"
					className="p-0"
					extra={[
						<Button
							key="1"
							className="w-120px text-capitalize"
							type="primary"
							icon={<PlusOutlined />}
							onClick={() =>
								props.actions.toggleModal({
									type: SHOP_MODAL.CREATE_EDIT_SHOP,
								})
							}
						>
							create
						</Button>,
					]}
				/>
				<Divider />
				<ShopTable {...props} />
			</div>
			<ShopForm {...props} />
			<CodeForm {...props} />
		</React.Fragment>
	);
};
