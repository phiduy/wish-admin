import React from 'react';
import { Input, Button, Modal, Form, Typography } from 'antd';
import { UserOutlined, ShopOutlined, KeyOutlined } from '@ant-design/icons';
import { IShopProps } from '../../model/IShopProps';
import { IShopInfo, IShopRecord, SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';

const { Title } = Typography;

interface IProps extends IShopProps {}

export const ShopForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const [temptInfoShop, setTemptInfoShop] = React.useState<IShopInfo | undefined>(undefined);
	const {
		isProcessing,
		isUpdatingShop,
		currentShop,
		toggleModalCreateEditShop,
		isUpdateShopSuccess,
	} = props.store.ShopPage;

	React.useEffect(() => {
		if (isUpdateShopSuccess) {
			props.actions.handleCurrentShop({
				type: 'detail',
				shopInfo: {
					...currentShop,
					shop: {
						...currentShop?.shop,
						name: temptInfoShop?.name as string,
						clientId: temptInfoShop?.clientId as string,
						clientSecret: temptInfoShop?.clientSecret as string,
					},
				} as IShopRecord,
			});
			props.actions.handleClear({ type: SHOP_CLEAR.UPDATE_SHOP });
		}
		if (currentShop) {
			formInstance.setFieldsValue({
				name: currentShop.shop.name,
				clientId: currentShop.shop.clientId,
				clientSecret: currentShop.shop.clientSecret,
			});
		}
		// eslint-disable-next-line
	}, [isUpdateShopSuccess]);

	const onFinish = (data: IShopInfo) => {
		if (isUpdatingShop) {
			setTemptInfoShop(data);
			props.actions.updateShop({
				shopId: currentShop?.shop?._id as string,
				shopInfo: data,
			});
		} else {
			props.actions.createShop(data);
		}
	};
	return (
		<Modal
			visible={toggleModalCreateEditShop}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: SHOP_MODAL.CREATE_EDIT_SHOP,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form
				form={formInstance}
				layout="vertical"
				onFinish={onFinish}
				scrollToFirstError={true}
			>
				<Title level={4}>Shop Information</Title>
				<Form.Item
					label="Name"
					name="name"
					className="w-100 mr-2"
					hasFeedback={true}
					rules={[
						{
							required: true,
							message: 'Name  is required',
						},
						{
							max: 256,
						},
						{
							min: 5,
						},
					]}
					children={
						<Input
							prefix={<ShopOutlined />}
							placeholder="Enter shop name"
							disabled={isProcessing}
						/>
					}
				/>

				<Form.Item
					label="Client Id"
					name="clientId"
					rules={[
						{
							required: true,
							message: 'Client Id  is required',
						},
						{
							max: 256,
						},
						{
							min: 10,
						},
					]}
				>
					<Input
						prefix={<UserOutlined />}
						placeholder="Enter client id"
						disabled={isProcessing}
					/>
				</Form.Item>

				<Form.Item
					label="Client Secret"
					name="clientSecret"
					rules={[
						{
							required: true,
							message: 'Client Secret  is required',
						},
						{
							max: 256,
						},
						{
							min: 10,
						},
					]}
					hasFeedback={true}
				>
					<Input.Password
						prefix={<KeyOutlined />}
						placeholder="Enter client secret"
						disabled={isProcessing}
					/>
				</Form.Item>

				<div className="d-flex w-100 justify-content-end">
					<Button
						className="text-capitalize ml-auto w-120px"
						type="primary"
						htmlType="submit"
						loading={isProcessing}
					>
						{isUpdatingShop ? 'Update' : 'Create'}
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
