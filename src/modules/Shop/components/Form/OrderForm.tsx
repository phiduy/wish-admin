import React from 'react';
import { Input, Button, Modal, Form, Select, Typography, message } from 'antd';
import { ShopOutlined, ReconciliationOutlined } from '@ant-design/icons';
import { IShopProps } from '../../model/IShopProps';
import { IOrder, SHOP_CLEAR, SHOP_MODAL } from '../../model/IShopState';

const { Title } = Typography;
const { Option } = Select;

interface IProps extends IShopProps {}

export const OrderForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const {
		isProcessing,
		isLoadingDeliveryCountries,
		isLoadingShippingCarrier,
		currentOrder,
		shippingCarriers,
		deliveryCountries,
		toggleModalShipOrder,
	} = props.store.ShopPage;
	React.useEffect(() => {
		if (currentOrder) {
			formInstance.setFieldsValue({
				orderId: currentOrder?.order_id,
			});
		}
		// eslint-disable-next-line
	}, [currentOrder]);

	const onFinish = (data: IOrder) => {
		if (currentOrder?.state === 'REFUNDED' || currentOrder?.state === 'SHIPPED') {
			props.actions.postModifyShippedOrder({
				shopId: currentOrder?.shopId._id as string,
				order: data,
			});
		} else {
			props.actions.postFullFillOrder({
				shopId: currentOrder?.shopId._id as string,
				order: data,
			});
		}
	};
	return (
		<Modal
			visible={toggleModalShipOrder}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: SHOP_MODAL.SHIP_ORDER,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
				message.destroy();
				props.actions.handleClear({ type: SHOP_CLEAR.ORDER });
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form
				form={formInstance}
				layout="vertical"
				initialValues={{
					countryCode: 'US',
				}}
				onFinish={onFinish}
				scrollToFirstError={true}
			>
				<Title level={4}>
					{currentOrder?.state === 'REFUNDED' || currentOrder?.state === 'SHIPPED'
						? 'Modify Shipping Information'
						: 'Order Information'}
				</Title>
				<Form.Item
					label="Order Id"
					name="orderId"
					className="w-100 mr-2"
					hasFeedback={true}
					rules={[
						{
							required: true,
						},
						{
							max: 256,
						},
					]}
					children={
						<Input
							prefix={<ShopOutlined />}
							placeholder="Ender order id"
							disabled={true}
						/>
					}
				/>

				<Form.Item
					label="Country"
					name="countryCode"
					rules={[
						{
							required: true,
							message: 'Country is required',
						},
					]}
				>
					<Select
						placeholder="Please Select"
						loading={isLoadingDeliveryCountries}
						showSearch={true}
						onChange={(value: string) => {
							props.actions.getShippingCarrierByLocale({
								shopId: currentOrder?.shopId._id as string,
								locale: value,
							});
							formInstance.setFieldsValue({
								shippingCarrier: '',
							});
						}}
					>
						{deliveryCountries.map((item: string) => (
							<Option value={item} key={item}>
								{item}
							</Option>
						))}
					</Select>
				</Form.Item>

				<Form.Item
					label="Shipping Carrier"
					name="shippingCarrier"
					rules={[
						{
							required: true,
							message: 'Shipping Carrier  is required',
						},
					]}
					hasFeedback={true}
				>
					<Select
						showSearch={true}
						placeholder="Select shipping carrier"
						loading={isLoadingShippingCarrier}
						disabled={isLoadingShippingCarrier}
					>
						{shippingCarriers.map((item: string) => (
							<Option value={item} key={item}>
								{item}
							</Option>
						))}
					</Select>
				</Form.Item>

				<Form.Item
					label="Tracking Number"
					name="trackingNumber"
					className="w-100 mr-2"
					hasFeedback={true}
					rules={[
						{
							required: true,
							message: 'Tracking Number  is required',
						},
						{
							max: 256,
						},
					]}
					children={
						<Input
							prefix={<ReconciliationOutlined />}
							placeholder="Ender tracking number"
						/>
					}
				/>

				<div className="d-flex w-100 justify-content-end">
					<Button
						className="text-capitalize ml-auto w-120px"
						type="primary"
						htmlType="submit"
						loading={isProcessing}
					>
						{currentOrder?.state === 'REFUNDED' || currentOrder?.state === 'SHIPPED'
							? 'Save'
							: 'Confirm'}
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
