import React from 'react';
import { Button, Input, Typography, Modal, Form } from 'antd';
import { IShopProps } from '../../model/IShopProps';
import { SHOP_MODAL } from '../../model/IShopState';

const { Title } = Typography;

export const CodeForm = (props: IShopProps) => {
	const [formInstance] = Form.useForm();
	const { toggleModalAddCode, isProcessing, currentShop } = props.store.ShopPage;

	const onFinish = (data: { code: string }) => {
		props.actions.addCode({
			code: data.code,
			shopId: currentShop?.shop._id as string,
		});
	};
	return (
		<Modal
			visible={toggleModalAddCode}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: SHOP_MODAL.ADD_CODE,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form form={formInstance} name="horizontal_login" onFinish={onFinish}>
				<Title level={4}>Add Code</Title>
				<Form.Item name="code" rules={[{ required: true, message: 'Code is required' }]}>
					<Input placeholder="Code" disabled={isProcessing} />
				</Form.Item>
				<div className="d-flex w-100 justify-content-end">
					<Button
						className="ml-auto w-120px"
						type="primary"
						htmlType="submit"
						loading={isProcessing}
					>
						Add Code
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
