import React from 'react';
import { Input, Button, Select, Modal, Form, Typography } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { IShopProps } from '../../model/IShopProps';
import { IShopStaff, SHOP_MODAL } from '../../model/IShopState';

const { Title } = Typography;
const { Option } = Select;

interface IProps extends IShopProps {}

export const StaffForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing, currentShop, toggleModalCreateEditStaff } = props.store.ShopPage;

	const onFinish = (data: IShopStaff) => {
		props.actions.createStaff({
			staff: data,
			shopId: currentShop?.shop._id as string,
		});
	};
	return (
		<Modal
			visible={toggleModalCreateEditStaff}
			onCancel={() => {
				formInstance.resetFields();
				props.actions.toggleModal({
					type: SHOP_MODAL.CREATE_EDIT_STAFF,
				});
			}}
			afterClose={() => {
				formInstance.resetFields();
			}}
			destroyOnClose={true}
			maskClosable={false}
			footer={null}
		>
			<Form
				form={formInstance}
				initialValues={{
					role: 'Staff',
				}}
				layout="vertical"
				onFinish={onFinish}
				scrollToFirstError={true}
			>
				<Title level={4}>Staff Information</Title>
				<Form.Item
					label="Email"
					name="email"
					rules={[
						{
							type: 'email',
						},
						{
							required: true,
							message: 'Email  is required',
						},
						{
							max: 256,
						},
					]}
				>
					<Input
						prefix={<MailOutlined />}
						placeholder="Enter your email"
						disabled={isProcessing}
					/>
				</Form.Item>

				<Form.Item
					label="Role"
					name="role"
					rules={[
						{
							required: true,
							message: 'Role  is required',
						},
					]}
				>
					<Select className="w-100">
						<Option value="Staff">Staff</Option>
					</Select>
				</Form.Item>

				<div className="d-flex w-100 justify-content-end">
					<Button
						className="text-capitalize ml-auto w-120px"
						type="primary"
						htmlType="submit"
						loading={isProcessing}
					>
						Create
					</Button>
				</div>
			</Form>
		</Modal>
	);
};
