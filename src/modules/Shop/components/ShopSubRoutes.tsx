import React from 'react';
import { Switch, Route, Redirect, useRouteMatch, RouteComponentProps } from 'react-router-dom';
import { IShopProps } from '../model/IShopProps';
import { ShopList } from './List';
import { ShopDetail } from './Detail';
import { AllOrders, ShopOrders } from './Order';

interface IProps extends IShopProps, RouteComponentProps {}

export const ShopSubRoutes: React.FC<IProps> = (props) => {
	const { url } = useRouteMatch();
	return (
		<Switch>
			<Route path={`${url}/list`} exact={true} children={() => <ShopList {...props} />} />
			<Route path={`${url}/detail/:shopId`} children={() => <ShopDetail {...props} />} />
			<Route path={`${url}/orders`} exact={true} children={() => <ShopOrders {...props} />} />
			<Route
				path={`${url}/orders/all`}
				exact={true}
				children={() => <AllOrders {...props} />}
			/>
			<Redirect from="*" to={`${url}/list`} />
		</Switch>
	);
};
