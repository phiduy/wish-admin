/**
 * @file index
 * Please import some config when register new module
 * Some base file neeed to update
 * * src/redux/rootReducers.js
 * * src/redux/rootSagas.js
 */

import sagas from './sagas';
import { reducer, name } from './reducers';
import { IShopState, IShopRecord, IShopStaff, IShopInfo, initialState } from './model/IShopState';

export { name, sagas, initialState };
export type { IShopState };
export type { IShopInfo };
export type { IShopStaff };
export type { IShopRecord };

export default reducer;
