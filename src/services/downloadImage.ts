import { message } from 'antd';

export const downloadImage = async (url: string, name: string) => {
	const file = await fetch(url)
		.then((r) => r.blob())
		.then((blobFile) => {
			return new File([blobFile], name, { type: 'image/png' });
		});
	const element = document.createElement('a');
	message.success('Download Successful');
	element.href = URL.createObjectURL(file);
	element.download = `${name}.png`;
	element.click();
};
